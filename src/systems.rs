use bevy::core_pipeline::tonemapping::DebandDither;
use bevy::core_pipeline::tonemapping::Tonemapping;
use bevy::prelude::*;
use bevy::math::Vec3Swizzles;

use bevy::render::camera::ScalingMode;
use bevy_rapier2d::prelude::*;

use crate::components::*;
use crate::entities::*;
use crate::renderer::PixelCamera;
use crate::renderer::atlas_material::AtlasMaterial;
use crate::renderer::atlas_tile::AtlasMaterialHandle;

pub fn snake_input_sprint(
	input: Res<Input<KeyCode>>,
	mut query: Query<&mut super::entities::SnakeSprint>,
) {
	// TODO: this should only affect the player snake? (not sure how to restrict the scope of this yet)

	for mut snake_sprint in &mut query {
		if input.pressed(KeyCode::Space) {
			snake_sprint.sprint();
		}
	}
}

pub fn camera_follow_snake(
	player_query: Query<(Entity, &SnakeHead, &Transform)>,
	mut camera_query: Query<&mut PixelCamera>,
) {
	let player_transform = match player_query.get_single() {
		Ok(t) => t.2.clone(),
		_ => return,
	};

	// get the first camera position to operate on
	let camera_xy = match camera_query.iter().next() {
		Some(camera) => camera.pos.clone(),
		_ => return,
	};
	let player_xy = player_transform.translation.xy();
	let camera_distance = camera_xy.distance(player_xy);
	let camera_velocity = (player_xy - camera_xy)
		.clamp_length(0., camera_distance.powf(2.));

	// determine the new position for all cameras
	let camera_pos = camera_xy + camera_velocity * 0.1;

	// move cameras to the new position
	for mut camera in camera_query.iter_mut() {
		camera.pos = camera_pos;
	}

	// scale camera according to the ViewportSize resource
	// orthographic_projection.scaling_mode = bevy::render::camera::ScalingMode::AutoMax {
	// 	max_width: viewport.0.x,
	// 	max_height: viewport.0.y,
	// };
}

/*pub fn restart_level(
	mut commands: Commands,
	level_query: Query<Entity, With<Handle<LdtkLevel>>>,
	input: Res<Input<KeyCode>>,
) {
	if input.just_pressed(KeyCode::R) {
		for level_entity in &level_query {
			commands.entity(level_entity).insert(Respawn);
		}
	}
}*/
