use bevy::prelude::*;

#[derive(Component, Clone, Debug, Default)]
pub struct Light {
	pub color: Color,
	pub radius: f32,
}

#[derive(Event)]
pub struct LightUniformsEvent {
	pub light_colors: [Vec4; 16],
	pub light_positions: [Vec4; 16],
	pub light_count: i32,
}

pub fn update_light_uniforms(
	mut ev_lights: EventWriter<LightUniformsEvent>,
	lights: Query<(&Light, &GlobalTransform)>, // TODO: (Changed<Light>, Changed<GlobalTransform>) - only if lights can have a uid
) {
	let mut light_colors = [Vec4::ZERO; 16];
	let mut light_positions = [Vec4::ZERO; 16];

	let mut i: usize = 0;
	for (light, transform) in lights.iter() {
		if i >= 16 {
			crate::macros::console_log!("Number of lights is greater than what can be passed to the shader!");
			break;
		}

		if light.color.a() == 0. || light.radius < 1. {
			continue;
		}

		let color = &mut light_colors[i];
		let position = &mut light_positions[i];

		color.x = light.color.r();
		color.y = light.color.g();
		color.z = light.color.b();
		color.w = light.color.a();

		let translation = transform.translation();
		position.x = translation.x;
		position.y = translation.y;
		position.z = translation.z;
		position.w = light.radius;

		i += 1;
	}

	ev_lights.send(LightUniformsEvent {
		light_colors,
		light_positions,
		light_count: i as i32,
	});
}

pub struct LightPlugin;

impl Plugin for LightPlugin {
	fn build(&self, app: &mut App) {
		app.add_event::<LightUniformsEvent>()
			.add_systems(Update, update_light_uniforms);
	}
}
