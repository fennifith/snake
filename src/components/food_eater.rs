use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

#[derive(Component, Clone, Debug, Default)]
pub struct Food {
	pub is_eaten: bool,
}

#[derive(Component, Clone, Debug, Default)]
pub struct FoodEater {
	pub count: u32,
	// cooldown_timer?
}

#[derive(Event)]
pub struct FoodEatenEvent {
	pub food: Food,
	pub food_eater: Entity,
}

pub fn update_food_eaters(
	mut commands: Commands,
	rapier_context: Res<RapierContext>,
	mut eater_query: Query<(Entity, &mut FoodEater)>,
	mut food_query: Query<(Entity, &mut Food)>,
	mut ev_food_eaten: EventWriter<FoodEatenEvent>,
) {
	for (food_entity, mut food) in food_query.iter_mut() {
		if food.is_eaten {
			continue;
		}

		for (eats_entity, mut eater) in eater_query.iter_mut() {
			// find an edge in rapier's contact graph
			if rapier_context.contact_pair(food_entity, eats_entity).is_none() {
				continue;
			}

			// increment the eaten count & set is_eaten=true
			food.is_eaten = true;
			eater.count += 1;
			// despawn the food entity
			commands.entity(food_entity).despawn();

			ev_food_eaten.send(FoodEatenEvent {
				food: food.clone(),
				food_eater: eats_entity.clone(),
			});
		}
	}
}
