use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

#[derive(Component, Default)]
pub struct InputMovement(pub Vec2);

pub fn update_input_movement(
	mut query: Query<&mut InputMovement>,
	input: Res<Input<KeyCode>>,
) {
	let mut dir = Vec2::splat(0.);

	if input.pressed(KeyCode::W) { dir.y += 1.; }
	if input.pressed(KeyCode::A) { dir.x += -1.; }
	if input.pressed(KeyCode::S) { dir.y += -1.; }
	if input.pressed(KeyCode::D) { dir.x += 1.; }

	if input.pressed(KeyCode::Up) { dir.y += 1.; }
	if input.pressed(KeyCode::Left) { dir.x += -1.; }
	if input.pressed(KeyCode::Down) { dir.y += -1.; }
	if input.pressed(KeyCode::Right) { dir.x += 1.; }

	for mut movement in &mut query {
		movement.0 = dir.normalize_or_zero();
	}
}
