use bevy::prelude::*;

#[derive(Component)]
pub struct YSorted(pub f32);

// TODO: this could use GlobalTransform, and update on Changed<GlobalTransform>1
pub fn update_y_sorted(
	mut query: Query<(&mut Transform, &YSorted), Or<(Changed<Transform>, Changed<YSorted>)>>,
) {
	for (mut transform, sort) in query.iter_mut() {
		let z_order = (transform.translation.y / 10000.0) % 0.999;
		transform.translation.z = sort.0.ceil() - z_order - 0.001;
	}
}
