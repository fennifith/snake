#![allow(dead_code)]
#![allow(unused_imports)]

use bevy::prelude::*;
use bevy::render::texture::ImageTextureLoader;
use bevy_common_assets::json::JsonAssetPlugin;
use bevy_rapier2d::prelude::*;
use crate::components::*;
use crate::entities::*;
use crate::renderer::*;
use crate::macros::wasm_console_log::*;
use crate::resources::AssetsLoading;

hypermod::hypermod!();

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Hash, States)]
pub enum AppState {
    #[default]
	Loading,
    Level,
}

fn main() {
	console_log!("HEY! LISTEN!");

	App::new()
		.add_plugins((
			bevy::diagnostic::LogDiagnosticsPlugin::default(),
			bevy::diagnostic::FrameTimeDiagnosticsPlugin::default(),
		))
		.add_plugins((
			DefaultPlugins,//.set(ImagePlugin::default_nearest()),
			noisy_bevy::NoisyShaderPlugin,
			crate::renderer::AtlasMaterialPlugin,
			crate::renderer::AtlasHeightMaterialPlugin,
			PixelShaderPlugin,
			RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(16.0),
			RapierDebugRenderPlugin {
				enabled: false,
				..Default::default()
			},
		))
		.init_asset_loader::<crate::NormalMappedImageTextureLoader>()
		.add_plugins((
			crate::components::LightPlugin,
			crate::entities::FogPlugin,
			crate::world::WorldPlugin,
			crate::entities::FeaturesPlugin,
			crate::entities::LanternPlugin,
		))
		.insert_resource(AssetsLoading::default())
		.add_state::<AppState>()
		.insert_resource(RapierConfiguration {
			gravity: Vec2::ZERO,
			..Default::default()
		})
		.insert_resource(Msaa::Off)
		.insert_resource(ClearColor(Color::BLACK))
		.add_event::<FoodEatenEvent>()
		.add_systems(Update, resources::check_assets_ready.run_if(in_state(AppState::Loading)))
		// general components for all entities
		.add_systems(Update, (
			update_input_movement,
			update_y_sorted,
		))
		.add_systems(Update, systems::snake_input_sprint)
		// .add_systems(OnEnter(AppState::Level), ldtk::spawn_level)
		// .add_systems(Update, ldtk::ldtk_systems())
		// .add_systems(OnExit(AppState::Level), ldtk::despawn_level)
		// entity interaction functions
		.add_systems(Startup, snake_setup)
		.add_systems(Startup, snake_setup_spawn_origin)
		// .add_systems(Startup, character_spawn_test)
		.add_systems(Update, (
			apple_systems(),
			block_systems(),
			snake_systems(),
			character_systems(),
			systems::camera_follow_snake,
			update_food_eaters,
		).run_if(in_state(AppState::Level)))
		.run();
}
