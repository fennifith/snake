use bevy::prelude::*;

lazy_static::lazy_static! {
	pub static ref TILE_TREE: IVec2 = IVec2::new(0, 12*16);
	pub static ref TILE_TREE_STUMP: IVec2 = IVec2::new(4*16, 12*16);

	pub static ref TILE_LEAF1: IVec2 = IVec2::new(2*16, 8*16);
	pub static ref TILE_LEAF2: IVec2 = IVec2::new(2*16 + 8, 8*16);
	pub static ref TILE_LEAF3: IVec2 = IVec2::new(2*16, 8*16 + 8);
	pub static ref TILE_LEAF4: IVec2 = IVec2::new(2*16 + 8, 8*16 + 8);

	pub static ref TILE_ROCK1: IVec2 = IVec2::new(0*16, 20*16);
	pub static ref TILE_ROCK2: IVec2 = IVec2::new(0*16 + 8, 20*16);
	pub static ref TILE_ROCK3: IVec2 = IVec2::new(0*16, 20*16 + 8);
	pub static ref TILE_ROCK4: IVec2 = IVec2::new(0*16 + 8, 20*16 + 8);

	pub static ref TILE_LOG: IVec2 = IVec2::new(8*16, 16*16);

	pub static ref TILE_WELL: IVec2 = IVec2::new(2*16, 9*16);

	pub static ref TILE_LANTERN1: IVec2 = IVec2::new(0*16, 18*16);
	pub static ref TILE_LANTERN2: IVec2 = IVec2::new(1*16, 18*16);
	pub static ref TILE_LANTERN3: IVec2 = IVec2::new(2*16, 18*16);

	pub static ref TILE_RUIN_PILLAR: IVec2 = IVec2::new(1*16, 19*16);
}

pub const LAYER_TERRAIN: f32 = -8.0;
pub const LAYER_ABOVE_TERRAIN: f32 = -7.0;
pub const LAYER_BELOW_SPRITES: f32 = 0.0;
pub const LAYER_SPRITES: f32 = 1.0;
pub const LAYER_ABOVE_SPRITES: f32 = 2.0;
pub const LAYER_WEATHER: f32 = 4.0;
