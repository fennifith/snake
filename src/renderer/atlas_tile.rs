use bevy::{
    core_pipeline::{clear_color::ClearColorConfig, tonemapping::{Tonemapping, DebandDither}},
    prelude::*,
    reflect::TypeUuid,
    render::{
        camera::{RenderTarget, ScalingMode},
        render_resource::{
            AsBindGroup, Extent3d, ShaderRef, TextureDescriptor, TextureDimension, TextureFormat,
            TextureUsages,
        },
		mesh::*,
    },
    sprite::{Material2d, Material2dPlugin, MaterialMesh2dBundle, Mesh2dHandle}, window::WindowResized,
};

use super::atlas_material::AtlasMaterial;

#[derive(Component, Clone)]
pub struct AtlasTile {
	pub quad: shape::Quad,
	pub uv: IVec2,
	pub flip: BVec2,
}

#[derive(Component, Resource, Clone, Default)]
pub struct AtlasMaterialHandle(pub Handle<AtlasMaterial>);

#[derive(Component, Clone, Default)]
pub struct AtlasMaterialDefaultHandle;

impl Default for AtlasTile {
	fn default() -> Self {
		Self {
			quad: shape::Quad::default(),
			uv: IVec2::ZERO,
			flip: BVec2::FALSE,
		}
	}
}

impl AtlasTile {
	fn into_mesh(&self) -> Mesh {
		let extent_x = self.quad.size.x / 2.0;
        let extent_y = self.quad.size.y / 2.0;

		// this adds 0.01 as a fix for unreliable webgl float precision on the wasm build
		// - otherwise, a "0.0" UV sometimes becomes "-0.00...1" and the shader's `floor(uv)` returns the wrong pixel
		let mut uv_start = self.uv.as_vec2() + 0.01; //  / atlas_size;
		let mut uv_end = uv_start + (self.quad.size);

		if self.flip.x {
			(uv_start.x, uv_end.x) = (uv_end.x, uv_start.x);
		}

		if self.flip.y {
			(uv_start.y, uv_end.y) = (uv_end.y, uv_start.y);
		}

		let normal: [f32; 3] = [if self.flip.x { -1. } else { 1. }, if self.flip.y { -1. } else { 1. }, 0.];

        let vertices = [
            ([-extent_x, -extent_y, 0.0], [uv_start.x, uv_end.y]),
            ([-extent_x, extent_y, 0.0], [uv_start.x, uv_start.y]),
            ([extent_x, extent_y, 0.0], [uv_end.x, uv_start.y]),
            ([extent_x, -extent_y, 0.0], [uv_end.x, uv_end.y]),
        ];

        let indices = Indices::U32(vec![0, 2, 1, 0, 3, 2]);

        let positions: Vec<_> = vertices.iter().map(|(p, _)| *p).collect();
        let normals: Vec<_> = vertices.iter().map(|_| normal.clone()).collect();
        let uvs: Vec<_> = vertices.iter().map(|(_, uv)| *uv).collect();

        let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
        mesh.set_indices(Some(indices));
        mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
        mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
        mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
        mesh
	}
}

pub fn create_material_handle(
	mut commands: Commands,
	default_material: ResMut<AtlasMaterialHandle>,
	without_material: Query<Entity, (With<AtlasMaterialDefaultHandle>, Without<Handle<AtlasMaterial>>)>,
) {
	for entity in without_material.iter() {
		let Some(mut entity_commands) = commands.get_entity(entity) else { continue };

		entity_commands
			.insert((
				default_material.0.clone(),
			));
	}
}

pub fn create_tile_mesh(
	mut commands: Commands,
	mut meshes: ResMut<Assets<Mesh>>,
	without_mesh: Query<(Entity, &AtlasTile), Changed<AtlasTile>>,
	transforms: Query<&Transform>,
) {
	for (entity, tile) in without_mesh.iter() {
		let mesh = tile.into_mesh();
		let mesh = meshes.add(mesh);

		let Some(mut entity_commands) = commands.get_entity(entity) else { continue };
		entity_commands
			// everything in MaterialMesh2dBundle, minus the material handle itself
			.insert((
				Mesh2dHandle(mesh),
				transforms.get(entity).cloned().unwrap_or_default(),
				GlobalTransform::default(),
				Visibility::default(),
				ComputedVisibility::default(),
			));
	}
}
