use bevy::{
    core_pipeline::{clear_color::ClearColorConfig, tonemapping::{Tonemapping, DebandDither}},
    prelude::*,
    reflect::{TypeUuid, TypePath},
    render::{
        camera::{RenderTarget, ScalingMode},
        render_resource::{
            AsBindGroup, Extent3d, ShaderRef, TextureDescriptor, TextureDimension, TextureFormat,
            TextureUsages,
        },
        texture::{BevyDefault, ImageSampler},
        view::RenderLayers,
    },
    sprite::{Material2d, Material2dPlugin, MaterialMesh2dBundle, Mesh2dHandle}, window::WindowResized,
};

use crate::{components::Light, macros::console_log};

use super::{AtlasMaterialHandle, HeightmapHandle};

#[derive(Component, Resource, Clone, Default)]
pub struct AtlasHeightMaterialHandle(pub Handle<AtlasHeightMaterial>);

pub struct AtlasHeightMaterialPlugin;

impl Plugin for AtlasHeightMaterialPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(AtlasHeightMaterialHandle::default())
            .add_plugins(Material2dPlugin::<AtlasHeightMaterial>::default())
            .add_systems(Startup, atlas_height_load_default)
            .add_systems(Update, (
                atlas_create_height_material,
            ));
    }
}

#[derive(AsBindGroup, TypeUuid, TypePath, Clone, Default)]
#[uuid = "7a61106c-e9b8-4dce-9660-099815d69628"]
pub struct AtlasHeightMaterial {
    #[texture(0)]
    pub tex_height: Handle<Image>,
}

impl Material2d for AtlasHeightMaterial {
    fn fragment_shader() -> ShaderRef {
        "shaders/atlas_height.wgsl".into()
    }
}

fn atlas_height_load_default(
    server: Res<AssetServer>,
    mut material_assets: ResMut<Assets<AtlasHeightMaterial>>,
    mut resource: ResMut<AtlasHeightMaterialHandle>,
) {
    let atlas_height = server.load("atlas/default.height.png");
    resource.0 = material_assets.add(AtlasHeightMaterial {
        tex_height: atlas_height,
    });
}

#[derive(Component, Clone, Debug, Default)]
pub struct AtlasHeightTile;

fn atlas_create_height_material(
    mut commands: Commands,
    query: Query<(Entity, &Mesh2dHandle), (With<super::AtlasTile>, With<AtlasHeightTile>, Changed<Mesh2dHandle>)>,
    res_material: Res<AtlasHeightMaterialHandle>,
) {
    for (atlas_entity, atlas_mesh) in query.iter() {
        let entity = commands.spawn((
            MaterialMesh2dBundle::<AtlasHeightMaterial> {
                mesh: atlas_mesh.clone(),
                material: res_material.0.clone(),
                ..default()
            },
            RenderLayers::layer(super::LAYER_HEIGHTMAP),
        )).id();

        commands.entity(atlas_entity)
            .add_child(entity);
    }
}
