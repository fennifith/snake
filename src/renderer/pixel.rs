use bevy::{
    core_pipeline::{clear_color::ClearColorConfig, tonemapping::{Tonemapping, DebandDither}},
    prelude::*,
    reflect::{TypeUuid, TypePath},
    render::{
        camera::{RenderTarget, ScalingMode},
        render_resource::{
            AsBindGroup, Extent3d, ShaderRef, TextureDescriptor, TextureDimension, TextureFormat,
            TextureUsages,
        },
        texture::BevyDefault,
        view::RenderLayers,
    },
    sprite::{Material2d, Material2dPlugin, MaterialMesh2dBundle}, window::WindowResized,
};

use crate::components::*;
use crate::console_log;

pub struct PixelShaderPlugin;

impl Plugin for PixelShaderPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(Material2dPlugin::<PixelShaderMaterial>::default())
            .insert_resource(HeightmapHandle::default()) // TODO: this might need to explicitly specify a Handle UUID
            .add_systems(Startup, setup)
            .add_systems(Update, update_camera_transforms)
            .add_systems(Update, update_image_to_window_size);
    }
}

// Used to determine the scene heightmap, only the red channel is taken into account
pub const LAYER_HEIGHTMAP: u8 = (RenderLayers::TOTAL_LAYERS - 2) as u8;

// The last layer to render, during pixel->screen upscaling and other postprocessing effects
//  - renders at the actual screen resolution
pub const LAYER_UPSCALE: u8 = (RenderLayers::TOTAL_LAYERS - 1) as u8;


/// To support window resizing, this fits an image to a windows size.
#[derive(Component)]
struct FitToWindowSize {
    image: Handle<Image>,
    material: Handle<PixelShaderMaterial>,
}

#[derive(Component)]
pub struct PixelCamera {
	pub pos: Vec2,
    pub size: UVec2,
}

#[derive(Component)]
pub struct ViewCamera;

#[derive(Resource, Default)]
pub struct HeightmapHandle(pub Handle<Image>);

fn get_image_size(window_size: UVec2) -> UVec2 {
    // aim to have 80px (TBD) diagonally, regardless of the window size
    let pixel_diag = 580f32;
    let window_diag = window_size.as_vec2().length();
    let ratio = pixel_diag / window_diag;

    (window_size.as_vec2() * ratio).as_uvec2()
}

fn setup(
    mut commands: Commands,
    windows: Query<&Window>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut images: ResMut<Assets<Image>>,
    mut pixel_shader_materials: ResMut<Assets<PixelShaderMaterial>>,
    mut res_heightmap_handle: ResMut<HeightmapHandle>,
) {
    let window = windows.single();

    // Compute the image texture size(s) based on the window scale
    let image_size = get_image_size(Vec2::new(window.width(), window.height()).as_uvec2());

    // This is the texture that will be rendered to.
    let mut image = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size: Extent3d {
                width: image_size.x,
                height: image_size.y,
                ..default()
            },
            dimension: TextureDimension::D2,
            format: TextureFormat::bevy_default(),
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::RENDER_ATTACHMENT,
            view_formats: &[],
        },
        ..default()
    };

    let mut image_heightmap = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size: Extent3d {
                width: image_size.x,
                height: image_size.y,
                ..default()
            },
            dimension: TextureDimension::D2,
            format: TextureFormat::bevy_default(),
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::RENDER_ATTACHMENT,
            view_formats: &[],
        },
        ..default()
    };

    image.resize(image.texture_descriptor.size.clone());
    image_heightmap.resize(image.texture_descriptor.size.clone());

    let image_handle = images.add(image);

    let image_heightmap_handle = images.add(image_heightmap);
    res_heightmap_handle.0 = image_heightmap_handle.clone();

    // heightmap camera, must render before main
    commands.spawn((
        Camera2dBundle {
            camera: Camera {
                order: 0,
                target: RenderTarget::Image(image_heightmap_handle.clone()),
                ..default()
            },
            projection: OrthographicProjection {
                scaling_mode: ScalingMode::WindowSize(1.),
                ..default()
            },
            // turn off unnecessary features
            tonemapping: Tonemapping::None,
            deband_dither: DebandDither::Disabled,
            transform: Transform::from_xyz(0., 0., 5.0).looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        },
        // Tag to change the camera view for application logic
        PixelCamera { pos: Vec2::ZERO, size: image_size },
        // Disable UI rendering for the first pass camera. This prevents double rendering of UI at
        // the cost of rendering the UI without any post processing effects.
        UiCameraConfig { show_ui: false },
        // Tag the camera to rescale the image/material if the window is resized
        FitToWindowSize {
            image: image_heightmap_handle.clone(),
            // TODO: this doesn't have a material to update when resized - it should affect all AtlasMaterials
            material: Handle::default(),
        },
        RenderLayers::layer(LAYER_HEIGHTMAP),
    ));

    // This material has the texture output from the main pass
    let material_handle = pixel_shader_materials.add(PixelShaderMaterial {
        source_image: image_handle.clone(),
    });

    // Main camera, second to render
    commands.spawn((
        Camera2dBundle {
            camera: Camera {
                order: 1,
                target: RenderTarget::Image(image_handle.clone()),
                ..default()
            },
            projection: OrthographicProjection {
                scaling_mode: ScalingMode::WindowSize(1.),
                ..default()
            },
            // turn off unnecessary features
            tonemapping: Tonemapping::None,
            deband_dither: DebandDither::Disabled,
            transform: Transform::from_xyz(0., 0., 5.0).looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        },
        // Tag to change the camera view for application logic
        PixelCamera { pos: Vec2::ZERO, size: image_size },
        // Disable UI rendering for the first pass camera. This prevents double rendering of UI at
        // the cost of rendering the UI without any post processing effects.
        UiCameraConfig { show_ui: false },
        // Tag the camera to rescale the image/material if the window is resized
        FitToWindowSize {
            image: image_handle.clone(),
            material: material_handle.clone(),
        },
        RenderLayers::default(),
    ));

    // Post processing 2d quad, with material using the render texture done by the main camera, with a custom shader.
    commands.spawn((
        MaterialMesh2dBundle {
            // this quad should be a bit larger than "1" to account for the view camera translation between frames
            mesh: meshes.add(Mesh::from(shape::Quad::new(Vec2::splat(1.5)))).into(),
            material: material_handle,
            ..default()
        },
        RenderLayers::layer(LAYER_UPSCALE),
    ));

    // The post-processing / upscaling camera.
    commands.spawn((
        Camera2dBundle {
            camera: Camera {
                // renders after the previous camera with order: 100
                order: 100,
                ..default()
            },
            projection: OrthographicProjection {
                scaling_mode: ScalingMode::Fixed { width: 1., height: 1. },
                ..default()
            },
            transform: Transform::from_xyz(0., 0., 5.0).looking_at(Vec3::ZERO, Vec3::Y),
            ..Camera2dBundle::default()
        },
        ViewCamera,
        RenderLayers::layer(LAYER_UPSCALE),
    ));

}

fn update_image_to_window_size(
    windows: Query<&Window>,
    mut image_events: EventWriter<AssetEvent<Image>>,
    mut images: ResMut<Assets<Image>>,
    mut pixel_shader_materials: ResMut<Assets<PixelShaderMaterial>>,
    resize_events: EventReader<WindowResized>,
    mut fit_to_window_size: Query<(&mut PixelCamera, &FitToWindowSize)>,
) {
    if resize_events.is_empty() {
        return;
    }

    let Ok(window) = windows.get_single() else { return };

    let image_size = get_image_size(Vec2::new(window.width(), window.height()).as_uvec2());

    for (mut pixel_camera, fit_to_window) in fit_to_window_size.iter_mut() {
        let image = images.get_mut(&fit_to_window.image).expect(
            "FitToWindowSize is referring to an Image, but this Image could not be found",
        );
        info!("resize to {:?}", image_size);
        pixel_camera.size = image_size;
        image.resize(Extent3d {
            width: image_size.x,
            height: image_size.y,
            ..default()
        });
        // Hack because of https://github.com/bevyengine/bevy/issues/5595
        image_events.send(AssetEvent::Modified {
            handle: fit_to_window.image.clone(),
        });
        pixel_shader_materials.get_mut(&fit_to_window.material);
    }
}


fn update_camera_transforms(
    camera_query: Query<(Entity, &PixelCamera), Changed<PixelCamera>>,
    view_camera_query: Query<Entity, With<ViewCamera>>,
	mut transforms: Query<&mut Transform>,
) {
    let Some((_, pixel_camera)) = camera_query.iter().next() else {
        return;
    };

    let pos_floor = pixel_camera.pos.floor();
    let pos_fract = pixel_camera.pos.fract();

    for (camera_entity, _) in camera_query.iter() {
        let mut camera_transform = transforms.get_mut(camera_entity).unwrap();
        camera_transform.translation.x = pos_floor.x;
        camera_transform.translation.y = pos_floor.y;
    }

    for view_camera in view_camera_query.iter() {
        let mut view_camera_transform = transforms.get_mut(view_camera).unwrap();
        view_camera_transform.translation.x = 1.5 * pos_fract.x / pixel_camera.size.x as f32;
        view_camera_transform.translation.y = 1.5 * pos_fract.y / pixel_camera.size.y as f32;
    }
}

#[derive(AsBindGroup, TypeUuid, TypePath, Clone)]
#[uuid = "f57c8dc1-0552-455d-952e-f90e2dac7fed"]
struct PixelShaderMaterial {
    #[texture(0)]
    #[sampler(1)]
    source_image: Handle<Image>,
}

impl Material2d for PixelShaderMaterial {
    fn fragment_shader() -> ShaderRef {
        "shaders/pixel.wgsl".into()
    }
}
