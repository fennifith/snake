use bevy::{
    core_pipeline::{clear_color::ClearColorConfig, tonemapping::{Tonemapping, DebandDither}},
    prelude::*,
    reflect::TypeUuid,
    render::{
        camera::{RenderTarget, ScalingMode},
        render_resource::{
            AsBindGroup, Extent3d, ShaderRef, TextureDescriptor, TextureDimension, TextureFormat,
            TextureUsages,
        },
		mesh::*,
    },
    sprite::{Material2d, Material2dPlugin, MaterialMesh2dBundle, Mesh2dHandle}, window::WindowResized,
};
use image::{GenericImageView, DynamicImage};
use super::*;
/*

fn create_mesh_for_tile(image: DynamicImage, tile: &AtlasTile) {
	let mut row_offsets: Vec<(u32, u32)> = vec![];

	for i_y in 0..(tile.quad.size.y as u32) {
		let y = tile.uv.y as u32 + i_y;


	}
}

pub fn create_tile_collider_exact(
	mut commands: Commands,
	without_collider: Query<(Entity, &AtlasTile, &AtlasMaterialHandle), Added<AtlasTile>>,
	materials: Res<Assets<AtlasMaterial>>,
	images: Res<Assets<Image>>,
) {
	for (entity, tile, material) in without_collider.iter() {
		let Some(material) = materials.get(&material.0) else { continue };
		let Some(image) = images.get(&material.tex_color) else { continue };

		let image_dynamic = image.clone().try_into_dynamic().unwrap();
		image_dynamic.get_pixel(x, y)
	}
}

 */
