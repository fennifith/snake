use bevy::{
    core_pipeline::{clear_color::ClearColorConfig, tonemapping::{Tonemapping, DebandDither}},
    prelude::*,
    reflect::{TypeUuid, TypePath},
    render::{
        camera::{RenderTarget, ScalingMode},
        render_resource::{
            AsBindGroup, Extent3d, ShaderRef, TextureDescriptor, TextureDimension, TextureFormat,
            TextureUsages,
        },
        texture::{BevyDefault, ImageSampler},
        view::RenderLayers,
    },
    sprite::{Material2d, Material2dPlugin, MaterialMesh2dBundle}, window::WindowResized,
};

use crate::{components::Light, macros::console_log};

use super::{AtlasMaterialHandle, HeightmapHandle};

pub struct AtlasMaterialPlugin;

impl Plugin for AtlasMaterialPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(AtlasMaterialHandle::default());
        app.add_plugins(Material2dPlugin::<AtlasMaterial>::default());
        app.add_systems(Startup, atlas_load_default);
        app.add_systems(Update, (
            super::create_tile_mesh,
            super::create_material_handle,
            atlas_update_lights,
            atlas_update_heightmap,
        ));
    }
}

fn atlas_load_default(
    server: Res<AssetServer>,
    mut material_assets: ResMut<Assets<AtlasMaterial>>,
    mut resource: ResMut<AtlasMaterialHandle>,
) {
    let atlas = server.load("atlas/default.png");
    let atlas_normal = server.load("atlas/default.normal.png");
    let atlas_height = server.load("atlas/default.height.png");
    resource.0 = material_assets.add(AtlasMaterial {
        tex_color: atlas,
        tex_normal: atlas_normal,
        tex_height: atlas_height,
        ..default()
    });
}

/*fn atlas_set_nearest_filter(
    mut ev_asset: EventReader<AssetEvent<Image>>,
    mut assets: ResMut<Assets<Image>>,
    material_handles: ResMut<AtlasMaterialHandles>,
) {
    for ev in ev_asset.iter() {
        match ev {
            AssetEvent::Created { handle } => {
                if *handle == material_handles.snace {
                    if let Some(texture) = assets.get_mut(&handle) {
                        texture.sampler_descriptor = ImageSampler::nearest();
                    }
                }
            },
            _ => {}
        }
    }
}*/

#[derive(AsBindGroup, TypeUuid, TypePath, Clone, Default)]
#[uuid = "3b0df962-fd83-4543-948d-81f53b50f1e5"]
pub struct AtlasMaterial {
    #[texture(0)]
    pub tex_color: Handle<Image>,
    #[texture(1)]
    pub tex_normal: Handle<Image>,
    #[texture(2)]
    pub tex_height: Handle<Image>,
    #[uniform(3)]
    pub light_colors: [Vec4; 16],
    #[uniform(4)]
    pub light_positions: [Vec4; 16],
    #[texture(5)]
    pub tex_view_height: Handle<Image>,
}

impl Material2d for AtlasMaterial {
    fn fragment_shader() -> ShaderRef {
        "shaders/atlas.wgsl".into()
    }
}

fn atlas_update_lights(
    mut materials: ResMut<Assets<AtlasMaterial>>,
    mut ev_lights: EventReader<crate::components::LightUniformsEvent>,
) {
    for event in ev_lights.iter() {
        for (_id, material) in materials.iter_mut() {
            material.light_colors.copy_from_slice(&event.light_colors);
            material.light_positions.copy_from_slice(&event.light_positions);
        }
    }
}

fn atlas_update_heightmap(
    mut materials: ResMut<Assets<AtlasMaterial>>,
    res_heightmap_handle: Res<HeightmapHandle>,
) {
    for (_id, material) in materials.iter_mut() {
        material.tex_view_height = res_heightmap_handle.0.clone();
    }
}
