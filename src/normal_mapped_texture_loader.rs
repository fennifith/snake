use bevy::asset::{AssetLoader, Error, LoadContext, LoadedAsset};
use bevy::render::texture::{CompressedImageFormats, Image, ImageType};
use bevy::utils::BoxedFuture;

// Custom loader as a temporary fix for https://github.com/bevyengine/bevy/issues/6371
// - otherwise, normal textures are loaded in sRGB color space, which significantly
//   offsets their normal vectors because rgb(128) no longer equals vec3(0.5) in the shader.

#[derive(Default)]
pub struct NormalMappedImageTextureLoader;

impl AssetLoader for NormalMappedImageTextureLoader {
	fn load<'a>(
		&'a self,
		bytes: &'a [u8],
		load_context: &'a mut LoadContext,
	) -> BoxedFuture<'a, Result<(), Error>> {
		Box::pin(async move {
			let dyn_img = Image::from_buffer(
				bytes,
				ImageType::Extension("png"),
				CompressedImageFormats::all(),
				false,
			)
			.unwrap();

			load_context.set_default_asset(LoadedAsset::new(dyn_img));
			Ok(())
		})
	}

	fn extensions(&self) -> &[&str] {
		&["normal.png", "height.png"]
	}
}
