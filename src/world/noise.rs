use bevy::prelude::*;
use noise::*;
use std::hash::Hash;
use std::rc::Rc;

pub struct BoxedNoiseFunction {
	seed: u32,
	function: NoiseFunction,
	noise_fn: Box<dyn NoiseFn<f64, 2>>,
}

impl Clone for BoxedNoiseFunction {
	fn clone(&self) -> Self {
		self.function.assemble(self.seed)
	}
}

impl BoxedNoiseFunction {
	pub fn evaluate(&self, pos: &IVec2) -> f64 {
		return self.noise_fn.get([pos.x as f64, pos.y as f64]);
	}
}

// This can derive(serde::Deserialize) if needed
#[derive(Clone)]
pub enum NoiseFunction {
	Abs(Box<NoiseFunction>),
	Add(Box<NoiseFunction>, Box<NoiseFunction>),
	Constant(f64),
	Multiply(Box<NoiseFunction>, Box<NoiseFunction>),
	Blend {
		a: Box<NoiseFunction>,
		b: Box<NoiseFunction>,
		control: Box<NoiseFunction>,
	},
	ScalePoint {
		value: Box<NoiseFunction>,
		x: f64,
		y: f64,
	},
	Billow {
		octaves: usize,
		frequency: f64,
		lacunarity: f64,
		persistence: f64,
	},
	Perlin,
	Worley,
	Simplex,
	Fbm {
		octaves: usize,
		frequency: f64,
		lacunarity: f64,
		persistence: f64,
	},
	Turbulence {
		source: Box<NoiseFunction>,
		frequency: f64,
		power: f64,
		roughness: usize,
	},
}

// Rust pls
// https://internals.rust-lang.org/t/f32-f64-should-implement-hash/5436/33
/*fn hash_f64<H>(value: &f64, state: &mut H) where H: std::hash::Hasher {
	let bits: u64 = unsafe { std::mem::transmute(*value) };
    let sign: i8 = if bits >> 63 == 0 { 1 } else { -1 };
    let mut exponent: i16 = ((bits >> 52) & 0x7ff) as i16;
    let mantissa = if exponent == 0 {
        (bits & 0xfffffffffffff) << 1
    } else {
        (bits & 0xfffffffffffff) | 0x10000000000000
    };

	exponent -= 1023 + 52;
	(mantissa, exponent, sign).hash(state);
}

impl Hash for NoiseFunction {
	fn hash<H>(&self, state: &mut H) where H: std::hash::Hasher {
		std::mem::discriminant(self).hash(state);
		match self {
			NoiseFunction::Abs(f) => {
				f.hash(state);
			},
			NoiseFunction::Add(a, b) => {
				a.hash(state);
				b.hash(state);
			},
			NoiseFunction::Blend { a, b, control } => {
				a.hash(state);
				b.hash(state);
				control.hash(state);
			},
			NoiseFunction::Multiply(a, b) => {
				a.hash(state);
				b.hash(state);
			},
			NoiseFunction::Constant(c) => {
				hash_f64(c, state);
			},
			NoiseFunction::ScalePoint { value, x, y } => {
				value.hash(state);
				hash_f64(x, state);
				hash_f64(y, state);
			},
			NoiseFunction::Billow { octaves, frequency, lacunarity, persistence } => {
				octaves.hash(state);
				hash_f64(frequency, state);
				hash_f64(lacunarity, state);
				hash_f64(persistence, state);
			},
			NoiseFunction::Perlin => {},
			NoiseFunction::Worley => {},
			NoiseFunction::Simplex => {},
			NoiseFunction::Fbm { octaves, frequency, lacunarity, persistence } => {
				octaves.hash(state);
				hash_f64(frequency, state);
				hash_f64(lacunarity, state);
				hash_f64(persistence, state);
			},
			NoiseFunction::Turbulence { source, power, frequency, roughness } => {
				source.hash(state);
				hash_f64(power, state);
				hash_f64(frequency, state);
				roughness.hash(state);
			},
		}
	}
}*/

impl NoiseFunction {
	pub fn assemble_fn(&self, seed: u32) -> Box<dyn NoiseFn<f64, 2>> {
		match self {
			NoiseFunction::Abs(f) => {
				Box::new(Abs::new(f.assemble_fn(seed)))
			},
			NoiseFunction::Add(a, b) => {
				Box::new(Add::new(
					a.assemble_fn(seed),
					b.assemble_fn(seed),
				))
			},
			NoiseFunction::Blend { a, b, control } => {
				Box::new(Blend::new(
					a.assemble_fn(seed),
					b.assemble_fn(seed),
					control.assemble_fn(seed),
				))
			},
			NoiseFunction::Multiply(a, b) => {
				Box::new(Multiply::new(
					a.assemble_fn(seed),
					b.assemble_fn(seed),
				))
			},
			NoiseFunction::Constant(c) => {
				Box::new(Constant::new(*c))
			},
			NoiseFunction::ScalePoint { value, x, y } => {
				Box::new(
					ScalePoint::new(value.assemble_fn(seed))
						.set_all_scales(*x, *y, 1., 1.)
				)
			},
			NoiseFunction::Billow { octaves, frequency, lacunarity, persistence } => {
				let mut noise = Billow::<Perlin>::new(seed);
				noise.octaves = *octaves;
				noise.frequency = *frequency;
				noise.lacunarity = *lacunarity;
				noise.persistence = *persistence;
				Box::new(noise)
			},
			NoiseFunction::Perlin => {
				Box::new(Perlin::new(seed))
			},
			NoiseFunction::Worley => {
				Box::new(Worley::new(seed))
			},
			NoiseFunction::Simplex => {
				Box::new(Simplex::new(seed))
			},
			NoiseFunction::Fbm { octaves, frequency, lacunarity, persistence } => {
				let mut noise = Fbm::<Perlin>::new(seed);
				noise.octaves = *octaves;
				noise.frequency = *frequency;
				noise.lacunarity = *lacunarity;
				noise.persistence = *persistence;
				Box::new(noise)
			},
			NoiseFunction::Turbulence { source, power, frequency, roughness } => {
				let noise: Turbulence::<Box<dyn NoiseFn<f64, 2>>, Perlin> = Turbulence::new(source.assemble_fn(seed))
					.set_power(*power)
					.set_frequency(*frequency)
					.set_roughness(*roughness);
				Box::new(noise)
			}
		}
	}

	pub fn assemble(&self, seed: u32) -> BoxedNoiseFunction {
		BoxedNoiseFunction {
			seed,
			function: self.clone(),
			noise_fn: self.assemble_fn(seed),
		}
	}
}

