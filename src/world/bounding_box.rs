use bevy::prelude::*;

#[derive(Clone, Debug, Default)]
pub struct BoundingBox {
    pub size: UVec2,
    pub position: IVec2,
}

impl BoundingBox {
    pub fn is_intersecting(&self, other: &BoundingBox) -> bool {
        let range_x = self.position.x..(self.position.x + self.size.x.max(1) as i32);
        let range_y = self.position.y..(self.position.y + self.size.y.max(1) as i32);

        (range_x.contains(&other.position.x) || range_x.contains(&(other.position.x + other.size.x as i32)))
        && (range_y.contains(&other.position.y) || range_y.contains(&(other.position.y + other.size.y as i32)))
    }

    pub fn translate(&self, translation: IVec2) -> Self {
        Self {
            size: self.size.clone(),
            position: self.position + translation,
        }
    }
}
