use bevy::prelude::*;
use rand::Rng;
use super::NoiseFunction;

pub fn grid_jitter_points(
    chunk_pos: IVec2,
    rng: &mut rand::rngs::SmallRng,
    grid_spacing: u32,
    grid_jitter: u32,
    chunk_size: i32,
) -> Vec<IVec2> {
    let mut ret = vec![];
    let chunk_max_vec = IVec2::splat(chunk_size - 1);
    let mut origin = (chunk_pos + 1) * chunk_size / (grid_spacing as i32) * (grid_spacing as i32) - (chunk_pos * chunk_size);

    if origin.x >= chunk_size {
        origin.x -= grid_spacing as i32;
    }

    if origin.y >= chunk_size {
        origin.y -= grid_spacing as i32;
    }

    if origin.x < 0 || origin.y < 0 || origin.x >= chunk_size || origin.y >= chunk_size {
        return ret;
    }

    let mut offset_x = origin.x;
    let mut offset_y = origin.y;
    while offset_y >= 0 {
        let jitter_x = rng.gen_range(-(grid_jitter as i32)..=(grid_jitter as i32));
        let jitter_y = rng.gen_range(-(grid_jitter as i32)..=(grid_jitter as i32));

        ret.push(IVec2::new(offset_x + jitter_x, offset_y + jitter_y)
            .clamp(IVec2::ZERO, chunk_max_vec));

        offset_x -= grid_spacing as i32;

        if offset_x < 0 {
            offset_x = origin.x;
            offset_y -= grid_spacing as i32;
        }
    }

    ret
}

#[cfg(test)]
mod test {
    use bevy::prelude::*;
    use rand::SeedableRng;
    use super::*;

    #[test]
    fn grid_spacing_returns_multiple_positions() {
        let mut rng = rand::rngs::SmallRng::seed_from_u64(1);

        let points = grid_jitter_points(IVec2::new(0, 0), &mut rng, 64, 0, 128);
    }

    #[test]
    fn grid_spacing_can_span_multiple_chunks() {
        let mut rng = rand::rngs::SmallRng::seed_from_u64(1);

        let points = grid_jitter_points(IVec2::new(0, 0), &mut rng, 513, 0, 128);
        assert_eq!(points, vec![IVec2::new(0, 0)]);

        let points = grid_jitter_points(IVec2::new(1, 0), &mut rng, 513, 0, 128);
        assert_eq!(points, vec![]);

        let points = grid_jitter_points(IVec2::new(2, 0), &mut rng, 513, 0, 128);
        assert_eq!(points, vec![]);

        let points = grid_jitter_points(IVec2::new(3, 0), &mut rng, 513, 0, 128);
        assert_eq!(points, vec![]);

        let points = grid_jitter_points(IVec2::new(4, 0), &mut rng, 513, 0, 128);
        assert_eq!(points, vec![IVec2::new(1, 0)]);
    }
}
