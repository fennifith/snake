use bevy::{
    core_pipeline::{clear_color::ClearColorConfig, tonemapping::{Tonemapping, DebandDither}},
    prelude::*,
    reflect::{TypeUuid, TypePath},
    render::{
        camera::{RenderTarget, ScalingMode},
        render_resource::{
            AsBindGroup, Extent3d, ShaderRef, TextureDescriptor, TextureDimension, TextureFormat,
            TextureUsages,
        },
        texture::{BevyDefault, ImageSampler},
        view::RenderLayers,
    },
    sprite::{Material2d, Material2dPlugin, MaterialMesh2dBundle}, window::WindowResized,
};

use crate::{components::Light, macros::console_log, renderer::HeightmapHandle};

pub struct ChunkMaterialPlugin;

impl Plugin for ChunkMaterialPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(Material2dPlugin::<ChunkMaterial>::default());
        app.add_systems(Update, (
            chunk_update_lights,
            chunk_update_heightmap,
        ));
    }
}

#[derive(AsBindGroup, TypeUuid, TypePath, Clone, Default)]
#[uuid = "a6152b4e-d4f4-4e47-a915-34e4e6f35863"]
pub struct ChunkMaterial {
    #[texture(0)]
    pub tex_color: Handle<Image>,
    #[texture(1)]
    pub tex_normal: Handle<Image>,
    #[texture(2)]
    pub tex_height: Handle<Image>,
    #[uniform(3)]
    pub light_colors: [Vec4; 16],
    #[uniform(4)]
    pub light_positions: [Vec4; 16],
    #[uniform(5)]
    pub light_count: i32,
    #[texture(6)]
    #[sampler(7)]
    pub tex_view_height: Handle<Image>,
}

impl Material2d for ChunkMaterial {
    fn fragment_shader() -> ShaderRef {
        "shaders/chunk.wgsl".into()
    }
}

fn chunk_update_lights(
    mut materials: ResMut<Assets<ChunkMaterial>>,
    mut ev_lights: EventReader<crate::components::LightUniformsEvent>,
) {
    for event in ev_lights.iter() {
        for (_id, material) in materials.iter_mut() {
            material.light_colors.copy_from_slice(&event.light_colors);
            material.light_positions.copy_from_slice(&event.light_positions);
            material.light_count = event.light_count;
        }
    }
}

fn chunk_update_heightmap(
    mut materials: ResMut<Assets<ChunkMaterial>>,
    res_heightmap_handle: Res<HeightmapHandle>,
) {
    for (_id, material) in materials.iter_mut() {
        material.tex_view_height = res_heightmap_handle.0.clone();
    }
}
