use std::collections::HashSet;
use bevy::{prelude::*, sprite::{MaterialMesh2dBundle, Mesh2dHandle}};
use rand::{SeedableRng, RngCore, Rng};
use futures_lite::{future, StreamExt};

use super::{ChunkLoader, TerrainImage, BoundingBox, grid_jitter_points, FeatureLocation};

#[derive(Clone, Component, Debug, Default)]
pub struct Chunk {
	pub pos: IVec2,
	pub seed: u64,
}

#[derive(Clone, Component, Debug)]
pub struct ChunkEntity {
	pub chunk_entity: Entity,
	pub pos: IVec2,
}

pub const CHUNK_SIZE: i32 = 128;

#[derive(Event, Clone)]
pub struct ChunkLoadEvent {
	pub entity: Entity,
	pub pos: IVec2,
}

#[derive(Event, Clone)]
pub struct ChunkUnloadEvent {
	pub entity: Entity,
	pub pos: IVec2,
}

#[cfg(target_arch = "wasm32")]
#[derive(Component)]
pub struct CreateTerrainImageTask;

#[cfg(not(target_arch = "wasm32"))]
#[derive(Component)]
pub struct CreateTerrainImageTask(bevy::tasks::Task<TerrainImage>);

impl CreateTerrainImageTask {
	#[cfg(target_arch = "wasm32")]
	fn new(_loader: ChunkLoader, _chunk_pos: IVec2) -> Self {
		// on WASM, chunk loading must be done synchronously, so we cannot use a task pool
		// so we just restrict it to one chunk load per frame instead.
		Self
	}

	#[cfg(not(target_arch = "wasm32"))]
	fn new(loader: ChunkLoader, chunk_pos: IVec2) -> Self {
		let pool = bevy::tasks::AsyncComputeTaskPool::get();
		let task = pool.spawn(async move {
			loader.create_terrain_image(chunk_pos)
		});
		Self(task)
	}
}

#[derive(Component, Clone, Debug)]
pub struct LoadsChunks {
	pub radius: i32,
}

pub fn chunk_nearby(
	mut commands: Commands,
	loader_query: Query<(&LoadsChunks, &Transform)>,
	chunk_query: Query<(Entity, &Chunk)>,
	loader: Res<ChunkLoader>,
	mut ev_load: EventWriter<ChunkLoadEvent>,
	mut ev_unload: EventWriter<ChunkUnloadEvent>,
) {
	let mut chunks_to_spawn = HashSet::new();

	for (loader, transform) in loader_query.iter() {
		let loader_chunk = Vec2::new(
			transform.translation.x / CHUNK_SIZE as f32,
			transform.translation.y / CHUNK_SIZE as f32,
		).round().as_ivec2();

		for x in -loader.radius..=loader.radius {
			for z in -loader.radius..=loader.radius {
				if x*x + z*z <= loader.radius.pow(2) {
					chunks_to_spawn.insert(IVec2::new(x, z) + loader_chunk);
				}
			}
		}
	}

	for (chunk_entity, chunk) in chunk_query.iter() {
		let is_within_radius = chunks_to_spawn.remove(&chunk.pos);

		if !is_within_radius {
			commands.entity(chunk_entity)
				.despawn_recursive();
		}

		ev_unload.send(ChunkUnloadEvent { entity: chunk_entity, pos: chunk.pos.clone() });
	}

	for chunk_pos in chunks_to_spawn.into_iter() {
		let task = CreateTerrainImageTask::new(loader.clone(), chunk_pos);

		let translation = (chunk_pos * CHUNK_SIZE).as_vec2();
		let chunk_entity = commands.spawn((
			Chunk {
				pos: chunk_pos.clone(),
				seed: loader.get_chunk_seed(chunk_pos.clone()),
			},
			task,
			SpatialBundle {
				transform: Transform::from_translation(Vec3::new(translation.x, translation.y, crate::LAYER_TERRAIN)),
				..default()
			},
		)).id();

		ev_load.send(ChunkLoadEvent { entity: chunk_entity, pos: chunk_pos.clone() });

		// get the generated features of all surrounding chunks
		let surrounding_features: Vec<FeatureLocation> = vec![
			loader.get_chunk_features(chunk_pos + IVec2::new(-1, 0)),
			loader.get_chunk_features(chunk_pos + IVec2::new(1, 0)),
			loader.get_chunk_features(chunk_pos + IVec2::new(0, -1)),
			loader.get_chunk_features(chunk_pos + IVec2::new(0, 1)),
		].concat();

		// collect the features to spawn for the current chunk
		let features = loader.get_chunk_features(chunk_pos)
			.into_iter()
			.filter(|f| {
				if let Some(bounding_box_f) = &f.bounding_box {
					// ensure that no surrounding features are intersecting
					!surrounding_features.iter()
						.filter_map(|sf| sf.bounding_box.as_ref())
						.any(|bounding_box_sf| bounding_box_f.is_intersecting(&bounding_box_sf))
				} else {
					true
				}
			})
			.collect::<Vec<_>>();

		for feature in features.into_iter() {
			commands.spawn((
				crate::entities::Feature {
					feature: feature.feature.clone(),
					seed: feature.feature_seed,
				},
				SpatialBundle {
					transform: Transform::from_translation(Vec3::new(
						feature.pos.x as f32,
						feature.pos.y as f32,
						0.0,
					)),
					..default()
				},
				ChunkEntity { chunk_entity, pos: chunk_pos.clone() },
			));
		}
	}
}

#[cfg(target_arch = "wasm32")]
pub fn chunk_load_async(
	mut commands: Commands,
	chunk_query: Query<(Entity, &Chunk, &Transform), With<CreateTerrainImageTask>>,
	loader: Res<ChunkLoader>,
	mut image_assets: ResMut<Assets<Image>>,
	mut material_assets: ResMut<Assets<super::ChunkMaterial>>,
	mesh_handle: Res<super::ChunkMeshHandle>,
) {
	// process at most one chunk load event per tick
	let Some((chunk_entity, chunk, chunk_transform)) = chunk_query.iter().next() else { return };

	let image = loader.create_terrain_image(chunk.pos);
	let terrain_image = image_assets.add(image.terrain_image);
	let terrain_normal = image_assets.add(image.terrain_normal);
	let terrain_height = image_assets.add(image.terrain_height);

	let terrain_material = material_assets.add(super::ChunkMaterial {
		tex_color: terrain_image,
		tex_normal: terrain_normal,
		tex_height: terrain_height,
		..default()
	});

	commands.entity(chunk_entity)
		.remove::<CreateTerrainImageTask>()
		.insert((
			MaterialMesh2dBundle::<super::ChunkMaterial> {
				mesh: Mesh2dHandle(mesh_handle.0.clone()),
				material: terrain_material,
				transform: chunk_transform.clone(),
				..default()
			},
		));
}

#[cfg(not(target_arch = "wasm32"))]
pub fn chunk_load_async(
	mut commands: Commands,
    mut transform_tasks: Query<(Entity, &Transform, &mut CreateTerrainImageTask)>,
	mut image_assets: ResMut<Assets<Image>>,
	mut material_assets: ResMut<Assets<super::ChunkMaterial>>,
	mesh_handle: Res<super::ChunkMeshHandle>,
) {
	for (chunk_entity, chunk_transform, mut task) in transform_tasks.iter_mut() {
		let Some(image) = future::block_on(future::poll_once(&mut task.0)) else { continue };
		let terrain_image = image_assets.add(image.terrain_image);
		let terrain_normal = image_assets.add(image.terrain_normal);
		let terrain_height = image_assets.add(image.terrain_height);

		let terrain_material = material_assets.add(super::ChunkMaterial {
			tex_color: terrain_image,
			tex_normal: terrain_normal,
			tex_height: terrain_height,
			..default()
		});

		commands.entity(chunk_entity)
			.remove::<CreateTerrainImageTask>()
			.insert((
				MaterialMesh2dBundle::<super::ChunkMaterial> {
					mesh: Mesh2dHandle(mesh_handle.0.clone()),
					material: terrain_material,
					transform: chunk_transform.clone(),
					..default()
				},
			));
	}
}

pub fn chunk_despawn_entities(
	mut commands: Commands,
	mut removals: RemovedComponents<Chunk>,
	entity_query: Query<(Entity, &ChunkEntity)>,
) {
	for removed_chunk in removals.iter() {
		for (entity, chunk) in entity_query.iter() {
			if chunk.chunk_entity == removed_chunk {
				commands.entity(entity)
					.despawn_recursive();
			}
		}
	}
}

pub fn chunk_debug(
	mut commands: Commands,
	chunk_query: Query<(Entity, &Chunk), (Added<Chunk>, Without<Sprite>)>,
) {
	for (chunk_entity, chunk) in chunk_query.iter() {
		let translation = (chunk.pos.clone() * CHUNK_SIZE).as_vec2();

		commands.entity(chunk_entity)
			.insert(SpriteBundle {
				sprite: Sprite {
					custom_size: Some(Vec2::splat(CHUNK_SIZE as f32)),
					color: Color::rgba(0., 0.2, 0.2, 1.),
					..default()
				},
				transform: Transform {
					translation: Vec3::new(translation.x, translation.y, -8.0),
					..default()
				},
				..default()
			});
	}
}
