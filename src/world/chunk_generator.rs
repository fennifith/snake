use std::{ops::Bound, collections::hash_map::DefaultHasher};
use std::hash::{Hash, Hasher};
use bevy::{prelude::*, render::render_resource::Extent3d};
use bevy_rapier2d::na::ComplexField;
use rand::{Rng, SeedableRng, RngCore};
use noise::*;
use super::{noise::NoiseFunction, NoiseCondition, CHUNK_SIZE, BoundingBox, bounding_box, BoxedNoiseFunction};
use crate::entities::FeatureType;

#[derive(Resource, Clone)]
pub struct ChunkLoader {
	// TODO: random noise functions, seed, etc.
	pub seed: u32,
	pub noise_surface: NoiseFunction,
	pub noise_normal: NoiseFunction,
	pub surfaces: Vec<SurfaceConfig>,
	pub features: Vec<FeatureConfig>,
}

impl ChunkLoader {
	pub fn get_chunk_seed(&self, chunk_pos: IVec2) -> u64 {
		let mut hasher = DefaultHasher::new();
		chunk_pos.hash(&mut hasher);
		self.seed.hash(&mut hasher);
		hasher.finish()
	}

	pub fn get_chunk_rng(&self, chunk_pos: IVec2) -> rand::rngs::SmallRng {
		rand::rngs::SmallRng::seed_from_u64(self.get_chunk_seed(chunk_pos))
	}

	pub fn get_chunk_features(&self, chunk_pos: IVec2) -> Vec<FeatureLocation> {
		let mut ret = vec![];
		let mut rng = self.get_chunk_rng(chunk_pos);

		let mut bounding_box_vec: Vec<BoundingBox> = vec![];

		for feature in self.features.iter() {
			let count = if feature.count.is_empty() {
				feature.count.start
			} else {
				rng.gen_range(feature.count.clone())
			};

			let mut grid_positions = super::grid_jitter_points(chunk_pos, &mut rng, feature.grid_spacing, feature.grid_jitter, super::CHUNK_SIZE);

			let feature_condition = feature.condition.as_ref()
				.map(|cond| cond.assemble(self.seed))
				.unwrap_or(std::rc::Rc::new(|_| true));

			for _ in 0..count {
				if grid_positions.len() == 0 { break; }
				let grid_index = rng.gen_range(0..grid_positions.len());
				let feature_pos = grid_positions.swap_remove(grid_index);
				let mut world_pos = (chunk_pos * super::CHUNK_SIZE) + feature_pos - (CHUNK_SIZE / 2);

				// ensure that the feature condition passes for the chosen location
				// (e.g. surface checks/etc)
				if !feature_condition(&world_pos) {
					continue;
				}

				if let Some(pos_offset) = feature.pos_offset {
					world_pos += pos_offset;
				}

				let bounding_box_opt = feature.bounding_box.as_ref()
					.map(|b| b.translate(world_pos));

				if let Some(bounding_box) = &bounding_box_opt {
					// if the bounding box intersects another feature, don't spawn it!
					if bounding_box_vec.iter().any(|b| b.is_intersecting(&bounding_box)) {
						continue;
					}
				}

				if let Some(bounding_box) = &bounding_box_opt {
					bounding_box_vec.push(bounding_box.clone());
				}

				ret.push(FeatureLocation {
					feature: feature.feature.clone(),
					feature_seed: rng.next_u64(),
					bounding_box: bounding_box_opt,
					pos: world_pos,
				});
			}
		}

		ret
	}
}

#[derive(Clone)]
pub struct SurfaceConfig {
	pub id: u8,
	pub condition: NoiseCondition,
	pub color: Color,
}

#[derive(Clone, Default)]
pub struct FeatureConfig {
	pub feature: FeatureType,
	pub count: std::ops::Range<u32>,
	pub condition: Option<NoiseCondition>,
	pub pos_offset: Option<IVec2>,
	pub bounding_box: Option<BoundingBox>,
	pub grid_spacing: u32,
	pub grid_jitter: u32,
}

#[derive(Clone)]
pub struct FeatureLocation {
	pub feature: FeatureType,
	pub feature_seed: u64,
	pub bounding_box: Option<BoundingBox>,
	pub pos: IVec2,
}

impl Default for ChunkLoader {
	fn default() -> Self {
		let seed = rand::thread_rng().next_u32();

		let noise_surface = NoiseFunction::Turbulence {
			source: Box::new(NoiseFunction::Add(
				Box::new(NoiseFunction::ScalePoint {
					value: Box::new(NoiseFunction::Fbm { octaves: 1, frequency: 2.0, lacunarity: 2.0, persistence: 10.0 }),
					x: 0.005,
					y: 0.01,
				}),
				Box::new(NoiseFunction::ScalePoint {
					value: Box::new(NoiseFunction::Perlin),
					x: 0.01,
					y: 0.02,
				}),
			)),
			frequency: 10.0,
			power: 2.0,
			roughness: 10,
		};

		let noise_water: NoiseFunction = NoiseFunction::ScalePoint {
			value: Box::new(NoiseFunction::Fbm { octaves: 2, frequency: 1.0, lacunarity: 2.0, persistence: 8.0 }),
			x: 0.001,
			y: 0.002,
		};

		let surface_water = SurfaceConfig {
			id: 0,
			condition: NoiseCondition::GreaterThan {
				a: noise_water.clone(),
				b: NoiseFunction::Constant(0.8),
			},
			color: Color::rgb_u8(5, 7, 22),
		};

		let surface_grass = SurfaceConfig {
			id: 1,
			condition: NoiseCondition::And(vec![
				// ensures that water is not present
				NoiseCondition::LessThan {
					a: noise_water.clone(),
					b: NoiseFunction::Constant(0.7),
				},
				NoiseCondition::GreaterThan {
					a: noise_surface.clone(),
					b: NoiseFunction::Constant(0.5),
				},
			]),
			color: Color::rgb_u8(9, 20, 13),
		};

		let surface_mud = SurfaceConfig {
			id: 2,
			condition: NoiseCondition::And(vec![
				NoiseCondition::Not(Box::new(surface_grass.condition.clone())),
				NoiseCondition::GreaterThan {
					a: noise_surface.clone(),
					b: NoiseFunction::Constant(-0.9),
				},
			]),
			color: Color::rgb_u8(20, 13, 9),
		};

		let surface_deep_mud = SurfaceConfig {
			id: 3,
			condition: NoiseCondition::Not(Box::new(surface_mud.condition.clone())),
			color: Color::rgb_u8(14, 9, 6),
		};

		ChunkLoader {
			seed,
			noise_surface: noise_surface.clone(),
			noise_normal: NoiseFunction::Blend {
				a: Box::new(NoiseFunction::ScalePoint {
					value: Box::new(NoiseFunction::Turbulence {
						source: Box::new(NoiseFunction::Perlin),
						frequency: 10.0,
						power: 0.2,
						roughness: 4,
					}),
					x: 0.005,
					y: 0.01,
				}),
				b: Box::new(NoiseFunction::ScalePoint {
					value: Box::new(NoiseFunction::Perlin),
					x: 0.0025,
					y: 0.005,
				}),
				control: Box::new(NoiseFunction::ScalePoint {
					value: Box::new(NoiseFunction::Simplex),
					x: 0.01,
					y: 0.02,
				}),
			},
			features: vec![
				FeatureConfig {
					feature: FeatureType::Tree,
					count: 0..10,
					condition: Some(surface_grass.condition.clone()),
					pos_offset: Some(IVec2::new(0, 12)),
					bounding_box: Some(BoundingBox {
						size: UVec2::new(16*4, 16*5),
						..default()
					}),
					grid_spacing: 32,
					grid_jitter: 12,
				},
				FeatureConfig {
					feature: FeatureType::Log,
					count: 2..10,
					condition: Some(surface_grass.condition.clone()),
					bounding_box: Some(BoundingBox {
						size: UVec2::new(16*2, 16),
						..default()
					}),
					grid_spacing: 84,
					grid_jitter: 32,
					..default()
				},
				FeatureConfig {
					feature: FeatureType::Lantern,
					count: 4..6,
					bounding_box: Some(BoundingBox {
						size: UVec2::splat(16),
						..default()
					}),
					grid_spacing: 156,
					grid_jitter: 32,
					..default()
				},
				FeatureConfig {
					feature: FeatureType::Well,
					count: 1..2,
					condition: Some(surface_mud.condition.clone()),
					bounding_box: Some(BoundingBox {
						size: UVec2::splat(16*2),
						..default()
					}),
					grid_spacing: 512,
					grid_jitter: 128,
					..default()
				},
				FeatureConfig {
					feature: FeatureType::RuinPillar,
					count: 0..5,
					condition: Some(surface_mud.condition.clone()),
					bounding_box: Some(BoundingBox {
						size: UVec2::splat(16),
						..default()
					}),
					grid_spacing: 128,
					grid_jitter: 64,
					..default()
				},
				FeatureConfig {
					feature: FeatureType::Leaf,
					count: 2..13,
					condition: Some(surface_grass.condition.clone()),
					bounding_box: Some(BoundingBox::default()),
					grid_spacing: 32,
					grid_jitter: 8,
					..default()
				},
				FeatureConfig {
					feature: FeatureType::Rock,
					count: 2..13,
					condition: Some(surface_mud.condition.clone()),
					bounding_box: Some(BoundingBox::default()),
					grid_spacing: 32,
					grid_jitter: 8,
					..default()
				},
			],
			surfaces: vec![
				surface_water,
				surface_grass,
				surface_mud,
				surface_deep_mud,
			],
		}
	}
}

pub struct TerrainImage {
	pub terrain: Vec<u8>,
	pub terrain_image: Image,
	pub terrain_normal: Image,
	pub terrain_height: Image,
}

impl ChunkLoader {
	pub fn create_terrain_image(&self, chunk_pos: IVec2) -> TerrainImage {
		let data_size = (CHUNK_SIZE * CHUNK_SIZE * 4) as usize;
		let mut data = vec![0; (CHUNK_SIZE * CHUNK_SIZE) as usize];
		let mut data_color = vec![0; data_size];
		let mut data_normal = vec![0; data_size];

		// pre-assemble the noise/condition fns to reduce processing
		let noise_normal = self.noise_normal.assemble(self.seed);

		let surface_fns = self.surfaces.iter()
			.map(|s| s.condition.assemble(self.seed))
			.collect::<Vec<_>>();

		for y in 0..CHUNK_SIZE {
			for x in 0..CHUNK_SIZE {
				let index = (y * CHUNK_SIZE + x) as usize;
				let index_4 = index * 4;

				let world_pos = (chunk_pos * CHUNK_SIZE) + (IVec2::new(x, y) - IVec2::splat(CHUNK_SIZE / 2)) * IVec2::new(1, -1);

				let surface_index = surface_fns.iter()
					.enumerate()
					.find(|(_, cond)| cond(&world_pos))
					.map(|(i, _)| i);

				let surface = surface_index.map(|i| self.surfaces.get(i))
					.flatten();

				let surface_id = surface.map(|s| s.id).unwrap_or(0);
				data[index] = surface_id;

				let surface_color = surface.map(|s| s.color).unwrap_or(Color::GREEN);
				data_color[index_4] = (surface_color.r() * 255.) as u8;
				data_color[index_4 + 1] = (surface_color.g() * 255.) as u8;
				data_color[index_4 + 2] = (surface_color.b() * 255.) as u8;
				data_color[index_4 + 3] = (surface_color.a() * 255.) as u8;

				let normal_value = noise_normal.evaluate(&world_pos);
				let normal_angle = normal_value * 2.0 * std::f64::consts::PI;
				let normal_intensity = 0.1;

				// approx: [-1, 1] * 127 + 127 => [-127, 126] + 127 => [0, 255]
				data_normal[index_4] = (normal_angle.sin() * normal_intensity * 127.5 + 127.5) as u8;
				data_normal[index_4 + 1] = (normal_angle.cos() * normal_intensity * 127.5 + 127.5) as u8;
				data_normal[index_4 + 2] = 255; // TODO: normal_intensity noise?
				data_normal[index_4 + 3] = 255;
			}
		}

		let terrain_image = Image::new(
			Extent3d {
				width: CHUNK_SIZE as u32,
				height: CHUNK_SIZE as u32,
				depth_or_array_layers: 1,
			},
			bevy::render::render_resource::TextureDimension::D2,
			data_color,
			bevy::render::render_resource::TextureFormat::Rgba8Unorm,
		);

		let terrain_normal = Image::new(
			Extent3d {
				width: CHUNK_SIZE as u32,
				height: CHUNK_SIZE as u32,
				depth_or_array_layers: 1,
			},
			bevy::render::render_resource::TextureDimension::D2,
			data_normal,
			bevy::render::render_resource::TextureFormat::Rgba8Unorm,
		);

		let terrain_height = Image::new(
			Extent3d {
				width: 1,
				height: 1,
				depth_or_array_layers: 1,
			},
			bevy::render::render_resource::TextureDimension::D2,
			vec![0, 0, 0, 0],
			bevy::render::render_resource::TextureFormat::Rgba8Unorm,
		);

		TerrainImage {
			terrain: data,
			terrain_image,
			terrain_normal,
			terrain_height,
		}
	}
}
