use bevy::prelude::*;
use bevy::sprite::Material2dPlugin;

use super::{ChunkLoader, CHUNK_SIZE};

pub struct WorldPlugin;

#[derive(Clone, Debug, Resource, Default)]
pub struct ChunkMeshHandle(pub Handle<Mesh>);

fn create_mesh() -> Mesh {
	let extent_x = CHUNK_SIZE as f32 / 2.0;
    let extent_y = CHUNK_SIZE as f32 / 2.0;

	// this is 0.01 as a fix for unreliable webgl float precision on the wasm build
	// - otherwise, a "0.0" UV sometimes becomes "-0.00...1" and the shader's `floor(uv)` returns the wrong pixel
	let uv_start = Vec2::splat(0.01 as f32);
	let uv_end = Vec2::splat(CHUNK_SIZE as f32);

    let vertices = [
        ([-extent_x, -extent_y, 0.0], [0.0, 0.0, 1.0], [uv_start.x, uv_end.y]),
        ([-extent_x, extent_y, 0.0], [0.0, 0.0, 1.0], [uv_start.x, uv_start.y]),
        ([extent_x, extent_y, 0.0], [0.0, 0.0, 1.0], [uv_end.x, uv_start.y]),
        ([extent_x, -extent_y, 0.0], [0.0, 0.0, 1.0], [uv_end.x, uv_end.y]),
    ];

    let indices = bevy::render::mesh::Indices::U32(vec![0, 2, 1, 0, 3, 2]);

    let positions: Vec<_> = vertices.iter().map(|(p, _, _)| *p).collect();
    let normals: Vec<_> = vertices.iter().map(|(_, n, _)| *n).collect();
    let uvs: Vec<_> = vertices.iter().map(|(_, _, uv)| *uv).collect();

    let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
    mesh.set_indices(Some(indices));
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
    mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
    mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
	mesh
}

fn setup_chunk_mesh(
	mut handle: ResMut<ChunkMeshHandle>,
	mut mesh_assets: ResMut<Assets<Mesh>>,
) {
	// let chunk_mesh = shape::Quad::flipped(Vec2::splat(CHUNK_SIZE as f32));
	handle.0 = mesh_assets.add(create_mesh());
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemSet)]
enum ChunkSet {
	ChunkLoad,
	ChunkUnload,
}

impl Plugin for WorldPlugin {
	fn build(&self, app: &mut App) {
		app.add_plugins(super::ChunkMaterialPlugin)
			.insert_resource(ChunkMeshHandle::default())
			.insert_resource(ChunkLoader::default())
			.add_event::<super::ChunkLoadEvent>()
			.add_event::<super::ChunkUnloadEvent>()
			.add_systems(Startup, setup_chunk_mesh)
			.add_systems(
				Last,
				(super::chunk_nearby, apply_deferred, super::chunk_load_async, super::chunk_despawn_entities, apply_deferred)
					.chain()
			);
	}
}
