use std::rc::Rc;
use bevy::prelude::*;
use super::NoiseFunction;

#[derive(Clone)]
pub enum NoiseCondition {
	And(Vec<NoiseCondition>),
	Or(Vec<NoiseCondition>),
	Not(Box<NoiseCondition>),
	GreaterThan {
		a: NoiseFunction,
		b: NoiseFunction,
	},
	LessThan {
		a: NoiseFunction,
		b: NoiseFunction,
	},
}

impl NoiseCondition {
	pub fn assemble(&self, seed: u32) -> Rc<dyn Fn(&IVec2) -> bool> {
		match self {
			NoiseCondition::And(vec) => {
				let vec_assembled = vec.iter()
					.map(|cond| cond.assemble(seed))
					.collect::<Vec<_>>();

				Rc::new(move |pos| vec_assembled.iter().all(|cond| cond(pos)))
			},
			NoiseCondition::Or(vec) => {
				let vec_assembled = vec.iter()
					.map(|cond| cond.assemble(seed))
					.collect::<Vec<_>>();

				Rc::new(move |pos| vec_assembled.iter().any(|cond| cond(pos)))
			},
			NoiseCondition::Not(cond) => {
				let cond_assembled = cond.assemble(seed);
				Rc::new(move |pos| !cond_assembled(pos))
			},
			NoiseCondition::GreaterThan { a, b } => {
				let a = a.assemble(seed);
				let b = b.assemble(seed);

				Rc::new(move |pos| a.evaluate(pos) > b.evaluate(pos))
			},
			NoiseCondition::LessThan { a, b } => {
				let a = a.assemble(seed);
				let b = b.assemble(seed);

				Rc::new(move |pos| a.evaluate(pos) < b.evaluate(pos))
			},
		}
	}

	pub fn evaluate(&self, seed: u32, pos: &IVec2) -> bool {
		match self {
			NoiseCondition::And(vec) => {
				vec.iter().all(|cond| cond.evaluate(seed, pos))
			},
			NoiseCondition::Or(vec) => {
				vec.iter().any(|cond| cond.evaluate(seed, pos))
			},
			NoiseCondition::Not(cond) => {
				!cond.evaluate(seed, pos)
			},
			NoiseCondition::GreaterThan { a, b } => {
				let a = a.assemble(seed).evaluate(pos);
				let b = b.assemble(seed).evaluate(pos);
				a > b
			},
			NoiseCondition::LessThan { a, b } => {
				let a = a.assemble(seed).evaluate(pos);
				let b = b.assemble(seed).evaluate(pos);
				a < b
			},
		}
	}
}
