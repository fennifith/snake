use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
use rand::prelude::*;
use crate::constants::*;
use crate::components::*;
use crate::renderer::AtlasMaterialDefaultHandle;
use crate::renderer::AtlasTile;
use std::f32::consts::{PI, FRAC_PI_4};

#[derive(Clone, Component, Debug)]
pub struct Feature {
	pub feature: FeatureType,
	pub seed: u64,
}

#[derive(Clone, Debug, Default)]
pub enum FeatureType {
	#[default]
	Unknown,
	Leaf,
	Rock,
	Tree,
	Log,
	Lantern,
	Well,
	RuinPillar,
}

pub fn spawn_tile(
	mut commands: Commands,
	query: Query<(Entity, &Feature, &Transform), Added<Feature>>,
) {
	for (feature_entity, feature, transform) in query.iter() {
		let mut rng = SmallRng::seed_from_u64(feature.seed);

		match feature.feature {
			FeatureType::Leaf => {
				commands.entity(feature_entity)
					.insert((
						Leaf { variant: rng.gen_range(0..4) },
						Transform {
							rotation: Quat::from_rotation_z(rng.gen_range(0.0..(2. * PI))),
							..*transform
						},
					));
			},
			FeatureType::Rock => {
				commands.entity(feature_entity)
					.insert((
						Rock { variant: rng.gen_range(0..4) },
					));
			},
			FeatureType::Tree => {
				commands.entity(feature_entity)
					.insert((
						Tree { stump: false },
					));
			},
			FeatureType::Log => {
				commands.entity(feature_entity)
					.insert((
						Log,
						Transform {
							rotation: Quat::from_rotation_z(rng.gen_range(-FRAC_PI_4..FRAC_PI_4)),
							..*transform
						},
					));
			},
			FeatureType::Lantern => {
				commands.entity(feature_entity)
					.insert((
						super::Lantern::default(),
					));
			},
			FeatureType::Well => {
				commands.entity(feature_entity)
					.insert((
						Well,
					));
			},
			FeatureType::RuinPillar => {
				commands.entity(feature_entity)
					.insert((
						RuinPillar,
					));
			},
			FeatureType::Unknown => {},
		}
	}
}

#[derive(Clone, Component, Debug, Default)]
pub struct Leaf {
	pub variant: u8,
}

pub fn leaf_tile(
	mut commands: Commands,
	query: Query<(Entity, &Leaf), Changed<Leaf>>,
) {
	for (entity, leaf) in query.iter() {
		let uv = match leaf.variant {
			0 => *TILE_LEAF1,
			1 => *TILE_LEAF2,
			2 => *TILE_LEAF3,
			_ => *TILE_LEAF4,
		};

		commands.entity(entity)
			.insert((
				AtlasTile {
					quad: shape::Quad::new(Vec2::new(8., 8.)),
					uv,
					flip: BVec2::FALSE,
				},
				AtlasMaterialDefaultHandle,
				YSorted(LAYER_BELOW_SPRITES),
				Collider::ball(2.0),
				AdditionalMassProperties::Mass(1.0),
				RigidBody::Dynamic,
				Velocity::default(),
				Damping { linear_damping: 10.0, angular_damping: 10.0 },
			));
	}
}

#[derive(Clone, Component, Debug, Default)]
pub struct Tree {
	pub stump: bool,
}

fn tree_tile(
	mut commands: Commands,
	query: Query<(Entity, &Tree), Changed<Tree>>,
) {
	for (entity, tree) in query.iter() {
		commands.entity(entity)
			.insert((
				AtlasTile {
					quad: shape::Quad::new(Vec2::new(16.*4., 16.*5.)),
					uv: if tree.stump {
						*TILE_TREE_STUMP
					} else {
						*TILE_TREE
					},
					flip: BVec2::FALSE,
				},
				AtlasMaterialDefaultHandle,
				YSorted(LAYER_ABOVE_SPRITES),
				Collider::compound(vec![
					(Vec2::new(0., -29.), 0., Collider::cuboid(14.0, 10.0)),
				]),
				RigidBody::Fixed,
			));
	}
}

#[derive(Clone, Component, Debug, Default)]
pub struct Well;

fn well_tile(
	mut commands: Commands,
	query: Query<(Entity, &Well), Changed<Well>>,
) {
	for (entity, _tree) in query.iter() {
		commands.entity(entity)
			.insert((
				AtlasTile {
					quad: shape::Quad::new(Vec2::new(16.*2., 16.*2.)),
					uv: *TILE_WELL,
					flip: BVec2::FALSE,
				},
				AtlasMaterialDefaultHandle,
				crate::renderer::AtlasHeightTile,
				YSorted(LAYER_ABOVE_SPRITES),
				Collider::compound(vec![
					(Vec2::new(0., -9.), 0., Collider::ball(5.0)),
				]),
				RigidBody::Fixed,
			));
	}
}

#[derive(Clone, Component, Debug, Default)]
pub struct RuinPillar;

fn ruin_pillar_tile(
	mut commands: Commands,
	query: Query<(Entity, &RuinPillar), Changed<RuinPillar>>,
) {
	for (entity, _ruin_pillar) in query.iter() {
		commands.entity(entity)
			.insert((
				AtlasTile {
					quad: shape::Quad::new(Vec2::new(16.*1., 16.*1.)),
					uv: *TILE_RUIN_PILLAR,
					flip: BVec2::FALSE,
				},
				AtlasMaterialDefaultHandle,
				crate::renderer::AtlasHeightTile,
				YSorted(LAYER_ABOVE_SPRITES),
				Collider::compound(vec![
					(Vec2::new(0., -6.), 0., Collider::cuboid(3., 2.)),
				]),
				RigidBody::Fixed,
			));
	}
}

#[derive(Clone, Component, Debug, Default)]
pub struct Rock {
	pub variant: u8,
}

pub fn rock_tile(
	mut commands: Commands,
	query: Query<(Entity, &Rock), Changed<Rock>>,
) {
	for (entity, rock) in query.iter() {
		let uv = match rock.variant {
			0 => *TILE_ROCK1,
			1 => *TILE_ROCK2,
			2 => *TILE_ROCK3,
			_ => *TILE_ROCK4,
		};

		commands.entity(entity)
			.insert((
				AtlasTile {
					quad: shape::Quad::new(Vec2::new(8., 8.)),
					uv,
					flip: BVec2::FALSE,
				},
				AtlasMaterialDefaultHandle,
				crate::renderer::AtlasHeightTile,
				YSorted(LAYER_ABOVE_TERRAIN),
			));
	}
}

#[derive(Clone, Component, Debug, Default)]
pub struct Log;

fn log_tile(
	mut commands: Commands,
	query: Query<(Entity, &Log), Changed<Log>>,
) {
	for (entity, _ruin_pillar) in query.iter() {
		commands.entity(entity)
			.insert((
				AtlasTile {
					quad: shape::Quad::new(Vec2::new(16.*2., 16.*1.)),
					uv: *TILE_LOG,
					flip: BVec2::FALSE,
				},
				AtlasMaterialDefaultHandle,
				crate::renderer::AtlasHeightTile,
				YSorted(LAYER_ABOVE_SPRITES),
				Collider::compound(vec![
					(Vec2::new(0., 1.), 0., Collider::cuboid(13., 4.)),
				]),
				RigidBody::Fixed,
			));
	}
}

pub struct FeaturesPlugin;

impl Plugin for FeaturesPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, (
			spawn_tile,
			leaf_tile,
			tree_tile,
			well_tile,
			ruin_pillar_tile,
			rock_tile,
			log_tile,
		));
    }
}
