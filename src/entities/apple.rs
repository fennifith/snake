use bevy::{prelude::*, ecs::schedule::SystemConfigs};
use bevy_rapier2d::prelude::*;
use crate::components::Food;

#[derive(Component, Clone, Debug, Default)]
pub struct Apple;

fn apple_sprite(
	mut commands: Commands,
	query: Query<(Entity, &Transform), Added<Apple>>,
) {
	for (entity, transform) in query.iter() {
		commands.entity(entity)
			.insert((
				SpriteBundle {
					sprite: Sprite {
						custom_size: Some(Vec2::splat(2.)),
						color: Color::rgba(1., 0., 0., 1.),
						..default()
					},
					transform: transform.clone(),
					..default()
				},
				Food::default(),
				Collider::cuboid(1., 1.),
				//Sensor,
			));
	}
}

pub fn apple_systems() -> SystemConfigs {
	(apple_sprite).chain()
}
