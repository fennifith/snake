use bevy::ecs::schedule::SystemConfigs;
use bevy::prelude::{*, shape::Quad};
use bevy::math::Vec3Swizzles;
use bevy_rapier2d::prelude::*;
// use bevy_tilemap::prelude::*;

use std::collections::{HashSet, VecDeque};
use std::time::Duration;
use crate::renderer::atlas_material::AtlasMaterial;
use crate::renderer::atlas_tile::AtlasMaterialHandle;
use crate::{components::*, renderer::atlas_tile::AtlasTile};

// const CHARACTER: &str = "atlas/character_1.png";
// const CHARACTER_METADATA: TilemapMetadata = TilemapMetadata {
//     size: TilemapSize { x: 5, y: 9 },
//     tile_size: TilemapTileSize { x: 16.0, y: 16.0 },
//     grid_size: TilemapGridSize { x: 16.0, y: 16.0 },
// };

#[derive(Clone, Component, Debug, Default)]
pub struct Character {
	pub facing: IVec2,
	pub frame: i32,
	pub frame_timer: Timer,
}

#[derive(Clone, Component, Debug, Default)]
pub struct CharacterAi {
	pub objective: IVec2,
}

pub fn character_spawn_test(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
	mut materials: ResMut<Assets<AtlasMaterial>>,
) {
	let character_material = materials.add(AtlasMaterial {
		tex_color: asset_server.load("atlas/character.png"),
		..default()
	});
	commands.spawn((
		Character::default(),
		character_material,
	));
}

pub fn character_colliders(
	mut commands: Commands,
	query: Query<(Entity, &Character), Added<Character>>,
) {
	for (entity, _character) in query.iter() {
		commands.entity(entity)
			.insert((
				Transform::default(),
				InputMovement::default(),
			))
			.insert((
				Collider::cuboid(4., 7.),
				RigidBody::KinematicPositionBased,
				KinematicCharacterController::default(),
				LockedAxes::ROTATION_LOCKED,
			));
	}
}

pub fn character_frame_movement(
	mut query: Query<(&mut Character, &InputMovement, &mut KinematicCharacterController)>,
	time: Res<Time>,
) {
	for (mut character, input, mut controller) in query.iter_mut() {
		let vel = input.0.length();
		let interval = if vel > 0.1 {
			(125. / vel) as u64
		} else {
			character.frame_timer.reset();
			character.frame = 0;
			continue;
		};

		character.frame_timer.set_duration(Duration::from_millis(interval));
		character.frame_timer.set_mode(TimerMode::Repeating);
		character.frame_timer.tick(time.delta());

		if character.frame_timer.just_finished() {
			// this isn't a bug, this should literally be 1 + (frame%8) to loop through frames 1-8 while running
			character.frame = 1 + character.frame % 8;
		}

		// determine facing from the velocity direction
		character.facing = IVec2::new(
			if input.0.x == 0. { 0 } else { input.0.x.signum() as i32 },
			if input.0.y == 0. { 0 } else { input.0.y.signum() as i32 },
		);

		// apply the current velocity to the character controller
		let translation = controller.translation.unwrap_or_default();
		controller.translation = Some(translation + input.0 * 32. * time.delta_seconds());
	}
}

pub fn character_tile(
	mut commands: Commands,
	query: Query<(Entity, &Character), Changed<Character>>,
) {
	for (entity, character) in query.iter() {
		let facing = character.facing;

		let row = if facing.x == 0 {
			if facing.y > 0 { 4 } else { 0 }
		} else if facing.y == 0 {
			2
		} else if facing.y > 0 {
			3
		} else {
			1
		};

		let flip = BVec2::new(character.facing.x < 0, false);

		commands.entity(entity)
			.insert((
				AtlasTile {
					quad: Quad::new(Vec2::new(16., 16.)),
					uv: IVec2::new(character.frame*16, row*16),
					flip,
				},
			));
	}
}

pub fn character_systems() -> SystemConfigs {
	(character_colliders, character_frame_movement, character_tile).chain()
}
