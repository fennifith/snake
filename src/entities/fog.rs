use bevy::prelude::*;
use bevy::render::render_resource::{AsBindGroup, ShaderRef};
use bevy::reflect::{TypeUuid, TypePath};
use bevy::render::mesh::{Indices, PrimitiveTopology};
use bevy::sprite::{Material2d, Material2dPlugin, MaterialMesh2dBundle, Mesh2dHandle};

#[derive(Clone, Component)]
pub struct Fog;

fn fog_setup(
    mut commands: Commands,
    mut material_assets: ResMut<Assets<FogMaterial>>,
    mut mesh_assets: ResMut<Assets<Mesh>>,
) {
    let material = material_assets.add(FogMaterial::default());

    let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
    mesh.set_indices(Some(Indices::U32(vec![0, 1, 2])));
    let mesh = mesh_assets.add(mesh);

    commands.spawn((
        Fog,
        MaterialMesh2dBundle {
            mesh: Mesh2dHandle(mesh),
            material,
            transform: Transform::from_xyz(0., 0., crate::LAYER_WEATHER),
            ..default()
        },
    ));
}

fn fog_update_lights(
    mut materials: ResMut<Assets<FogMaterial>>,
    mut ev_lights: EventReader<crate::components::LightUniformsEvent>,
) {
    for event in ev_lights.iter() {
        for (_id, material) in materials.iter_mut() {
            material.light_colors.copy_from_slice(&event.light_colors);
            material.light_positions.copy_from_slice(&event.light_positions);
        }
    }
}

#[derive(AsBindGroup, TypeUuid, TypePath, Clone, Default)]
#[uuid = "a2b4d4f7-7a19-40d8-a7f4-87fb3356e8a1"]
pub struct FogMaterial {
    #[uniform(0)]
    pub light_colors: [Vec4; 16],
    #[uniform(1)]
    pub light_positions: [Vec4; 16],
}

impl Material2d for FogMaterial {
    fn vertex_shader() -> ShaderRef {
        "shaders/fog.wgsl".into()
    }
    
    fn fragment_shader() -> ShaderRef {
        "shaders/fog.wgsl".into()
    }
}

pub struct FogPlugin;

impl Plugin for FogPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(Material2dPlugin::<FogMaterial>::default())
            .add_systems(Startup, fog_setup)
            .add_systems(Update, fog_update_lights);
    }
}
