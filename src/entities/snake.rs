use bevy::ecs::schedule::SystemConfigs;
use bevy::prelude::*;
use bevy::math::Vec3Swizzles;
use bevy::sprite::{MaterialMesh2dBundle, Mesh2dHandle};
use bevy_rapier2d::prelude::*;
use bevy_rapier2d::geometry::Group;

use std::collections::VecDeque;
use std::time::Duration;

use crate::LAYER_SPRITES;
use crate::components::{InputMovement, FoodEater, FoodEatenEvent};

pub const COLLISION_GROUP_SNAKE: Group = Group::GROUP_1;

#[derive(Clone, Component, Debug, Default)]
pub struct SnakeSpawn {
	pub snake: Option<Entity>,
}

#[derive(Clone, Component, Debug, Default)]
pub struct SnakeHead {
	pub speed: f32,
	pub movement_timer: Timer,
	pub velocity: Vec2,
	pub direction: IVec2,
	pub segments: VecDeque<Entity>,
	pub segments_num: usize,
}

#[derive(Clone, Component, Debug, Default)]
pub struct SnakeSegment;

#[derive(Clone, Component, Debug, Default)]
pub struct SnakeSprint {
	pub speed: f32,
	pub sprint_timer: Timer,
	pub sprint_cooldown: Timer,
}

impl SnakeSprint {
	pub fn sprint(&mut self) {
		if self.sprint_cooldown.finished() {
			self.sprint_cooldown.reset();
			self.sprint_timer.reset();
		}
	}
}

pub fn snake_setup_spawn_origin(
	mut commands: Commands,
) {
	commands.spawn((
		SnakeSpawn::default(),
		Transform::from_translation(Vec3::new(0., 0., LAYER_SPRITES)),
	));
}

fn snake_spawn(
	mut commands: Commands,
	mut snake_spawn: Query<(Entity, &Transform, &mut SnakeSpawn)>,
) {
	for (_spawn_entity, spawn_transform, mut spawn) in snake_spawn.iter_mut() {
		// if the snake exists, don't respawn
		if let Some(e) = spawn.snake {
			if commands.get_entity(e).is_some() {
				continue;
			}
		}

		// otherwise, spawn a new snake...
		let snake_entity = commands
			.spawn(SnakeHead {
				speed: 1.,
				segments_num: 10,
				..SnakeHead::default()
			})
			.insert(SpatialBundle {
				transform: spawn_transform.clone(),
				..default()
			})
			.insert((
				SnakeSprint {
					speed: 10.,
					sprint_timer: Timer::from_seconds(0.1, TimerMode::Once),
					sprint_cooldown: Timer::from_seconds(0.7, TimerMode::Once),
				},
				FoodEater::default(),
			))
			.insert((
				Collider::cuboid(0.5, 0.5),
				CollisionGroups::new(COLLISION_GROUP_SNAKE, !COLLISION_GROUP_SNAKE),
				RigidBody::KinematicPositionBased,
				InputMovement::default(),
				Velocity::default(),
				ColliderMassProperties::MassProperties(MassProperties {
					local_center_of_mass: Vec2::ZERO,
					mass: 0.5,
					principal_inertia: 1.,
				}),
				crate::components::Light {
					color: Color::rgba_u8(100, 255, 100, 100),
					radius: 100.,
				},
				crate::world::LoadsChunks {
					radius: 3,
				},
				super::CanLightLanterns,
			))
			.id();


		/*let light_entity = commands.spawn(PointLightBundle {
			point_light: PointLight {
				color: Color::rgba(0.5, 1., 0.5, 1.),
				intensity: 500.0,
				shadows_enabled: false,
				..default()
			},
			transform: Transform::from_xyz(0., 0., 0.5),
			..default()
		}).id();CollisionGroups::new(SNAKE_GROUP, !SNAKE_GROUP),

		commands.entity(snake_entity).add_child(light_entity);*/

		println!("The snake is here.");
		spawn.snake = Some(snake_entity);
	}
}

fn snake_sum_speed(
	mut heads: Query<(&mut SnakeHead, &mut SnakeSprint, &InputMovement)>,
	time: Res<Time>,
) {
	for (mut snake, mut snake_sprint, input) in heads.iter_mut() {
		snake.velocity = snake.velocity.lerp(input.0, (4. * time.delta_seconds()).clamp(0., 1.));

		// update direction, only if the input_velocity is nonzero
		if input.0.length() != 0. {
			snake.direction = IVec2::select(input.0.cmpne(Vec2::ZERO), input.0.signum().as_ivec2(), IVec2::ZERO);
		}

		let mut speed = if snake.velocity.length() != 0. {
			snake.speed
			/ (1. + 0.05 * snake.segments_num as f32)
			* snake.velocity.length()
			* (1. / snake.direction.as_vec2().length().min(1.))
		} else { 0. };

		snake_sprint.sprint_timer.tick(time.delta());
		snake_sprint.sprint_cooldown.tick(time.delta());
		speed = (snake_sprint.speed * snake_sprint.sprint_timer.percent_left())
			+ (speed * snake_sprint.sprint_timer.percent());
		if !snake_sprint.sprint_cooldown.finished() {
			speed *= snake_sprint.sprint_cooldown.percent_left();
		}

		let movement_interval = (15. / speed) as u64;
		snake.movement_timer.set_duration(Duration::from_millis(movement_interval));
		snake.movement_timer.set_mode(TimerMode::Repeating);

		if snake.movement_timer.elapsed() > snake.movement_timer.duration() {
			snake.movement_timer.reset();
		}
	}
}

fn snake_movement(
	mut heads: Query<(Entity, &mut SnakeHead, &Collider), Without<SnakeRagdoll>>,
	mut transforms: Query<&mut Transform>,
	rigid_body_query: Query<&RigidBody>,
	rapier_context: Res<RapierContext>,
	time: Res<Time>,
) {
	// advance all snakes based on head direction
	for (head_entity, mut head, head_collider) in heads.iter_mut() {
		head.movement_timer.tick(time.delta());

		// if the head isn't moving, don't do anything
		if head.direction.abs().max_element() == 0 {
			continue;
		}

		for _ in 0..head.movement_timer.times_finished_this_tick() {
			let mut head_pos = transforms.get_mut(head_entity).unwrap();
			let mut segment_pos_prev = head_pos.translation.xy().floor() + Vec2::splat(0.5);
			let head_pos_next = segment_pos_prev + head.direction.as_vec2();

			// determine if head_pos_next collides with a wall
			let mut is_blocked = false;
			rapier_context.intersections_with_shape(
				head_pos_next, 0., head_collider,
				QueryFilter::default(),
				|entity| {
					match rigid_body_query.get(entity) {
						// if the entity is fixed, it should block movement
						Ok(RigidBody::Fixed) => {
							is_blocked = true;
							false
						},
						_ => true,
					}
				}
			);

			// if so, prevent movement in that direction
			if is_blocked {
				continue;
			}

			// move the head one unit in its direction
			head_pos.translation.x = head_pos_next.x;
			head_pos.translation.y = head_pos_next.y;
			head_pos.rotation = Quat::IDENTITY;

			// move all snake segments forwards in-place
			// - this ensures that the ordering remains the same so that the ragdoll joint links don't need updating
			for segment_entity in head.segments.iter() {
				let mut segment_pos = transforms.get_mut(*segment_entity).unwrap();

				let segment_pos_tmp = segment_pos.translation.xy().floor() + Vec2::splat(0.5);

				segment_pos.translation.x = segment_pos_prev.x;
				segment_pos.translation.y = segment_pos_prev.y;
				segment_pos.rotation = Quat::IDENTITY;

				segment_pos_prev = segment_pos_tmp;
			}
		}
	}
}

fn snake_eat(
	mut ev_food_eaten: EventReader<FoodEatenEvent>,
	mut query: Query<&mut SnakeHead>,
) {
	for ev in ev_food_eaten.iter() {
		let Ok(mut snake) = query.get_mut(ev.food_eater) else { continue };

		// grow the snake
		snake.segments_num += 1;
	}
}

fn snake_spawn_segments(
	mut commands: Commands,
	mut query: Query<(Entity, &mut SnakeHead, &Transform, Option<&SnakeRagdoll>), Changed<SnakeHead>>,
) {
	for (snake_entity, mut snake_head, snake_transform, is_ragdoll) in query.iter_mut() {
		// if the snake needs to grow a segment, append one
		if snake_head.segments.len() >= snake_head.segments_num {
			continue;
		}

		let segment_joint = RevoluteJointBuilder::new()
			.local_anchor1(Vec2::new(0., -0.5))
			.local_anchor2(Vec2::new(0., 0.5));

		let segment_joint_entity = snake_head.segments.back().unwrap_or(&snake_entity);

		let segment_entity = commands
			.spawn(SnakeSegment::default())
			.insert((
				SpatialBundle {
					transform: snake_transform.clone(),
					..default()
				},
				Collider::cuboid(0.5, 0.5),
				CollisionGroups::new(COLLISION_GROUP_SNAKE, !COLLISION_GROUP_SNAKE),
				if is_ragdoll.is_some() { RigidBody::Dynamic } else { RigidBody::KinematicPositionBased },
				ImpulseJoint::new(segment_joint_entity.clone(), segment_joint),
				ColliderMassProperties::MassProperties(MassProperties {
					local_center_of_mass: Vec2::ZERO,
					mass: 0.1,
					principal_inertia: 0.2,
				}),
				Damping { linear_damping: 0.5, angular_damping: 3.0 },
			))
			.id();

		snake_head.segments.push_back(segment_entity);
	}
}

#[derive(Resource)]
pub struct SnakeMaterial {
	mesh: Handle<Mesh>,
	material: Handle<ColorMaterial>,
}

pub fn snake_mesh(
	mut commands: Commands,
	snake_head_query: Query<(Entity, &SnakeHead), Added<SnakeHead>>,
	snake_part_query: Query<(Entity, &SnakeSegment), Added<SnakeSegment>>,
	resource: Res<SnakeMaterial>,
) {
	for (entity, _snake) in snake_head_query.iter() {
		commands.entity(entity)
			.insert((
				Mesh2dHandle(resource.mesh.clone()),
				resource.material.clone(),
			));
	}

	for (entity, _snake) in snake_part_query.iter() {
		commands.entity(entity)
			.insert((
				Mesh2dHandle(resource.mesh.clone()),
				resource.material.clone(),
			));
	}
}

pub fn snake_setup(
	mut commands: Commands,
	mut meshes: ResMut<Assets<Mesh>>,
	mut materials: ResMut<Assets<ColorMaterial>>,
) {
	commands.insert_resource(SnakeMaterial {
		mesh: meshes.add(shape::Quad::new(Vec2::new(2., 2.)).into()),
		material: materials.add(ColorMaterial::from(Color::rgba(0.5, 1., 0.5, 1.))),
	});
}

#[derive(Component, Clone, Debug, Default)]
pub struct SnakeRagdoll;

pub fn snake_toggle_ragdoll(
	mut commands: Commands,
	query: Query<(Entity, &SnakeHead, Option<&SnakeRagdoll>)>,
	input: Res<Input<KeyCode>>,
) {
	if input.just_pressed(KeyCode::R) {
		for (snake_entity, _snake, is_ragdoll) in query.iter() {
			if is_ragdoll.is_some() {
				commands.entity(snake_entity).remove::<SnakeRagdoll>();
			} else {
				commands.entity(snake_entity).insert(SnakeRagdoll);
			}
		}
	}
}

pub fn snake_ragdoll(
	mut commands: Commands,
	query: Query<&SnakeHead>,
	with_ragdoll: Query<(Entity, &SnakeHead), Added<SnakeRagdoll>>,
	mut without_ragdoll: RemovedComponents<SnakeRagdoll>,
) {
	// when SnakeRagdoll is added, set to RigidBody::Dynamic
	for (snake_entity, snake) in with_ragdoll.iter() {
		commands.entity(snake_entity)
			.insert(RigidBody::Dynamic);

		for entity in snake.segments.iter() {
			commands.entity(*entity)
				.insert(RigidBody::Dynamic);
		}
	}

	// when SnakeRagdoll is removed, set to RigidBody::Kinematic
	for snake_entity in without_ragdoll.iter() {
		let Ok(snake) = query.get(snake_entity) else { continue };

		commands.entity(snake_entity)
			.insert(RigidBody::KinematicPositionBased);

		for entity in snake.segments.iter() {
			commands.entity(*entity)
				.insert(RigidBody::KinematicPositionBased);
		}
	}
}

pub fn snake_ragdoll_force(
	mut commands: Commands,
	heads: Query<(Entity, &SnakeHead), With<SnakeRagdoll>>,
) {
	for (snake_entity, snake) in heads.iter() {
		commands.entity(snake_entity)
			.insert(ExternalForce {
				force: snake.velocity.clone() * 32.,
				torque: 0.0,
			});
	}
}

pub fn snake_systems() -> SystemConfigs {
	(snake_spawn, snake_mesh, snake_sum_speed, snake_movement, snake_eat, snake_spawn_segments, snake_ragdoll, snake_ragdoll_force, snake_toggle_ragdoll)
		.chain()
}
