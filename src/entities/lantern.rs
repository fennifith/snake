use bevy::prelude::*;
use bevy::time::Stopwatch;
use bevy_rapier2d::prelude::*;
use crate::renderer::AtlasMaterialDefaultHandle;
use crate::renderer::AtlasTile;
use crate::components::YSorted;
use crate::components::Light;

#[derive(Clone, Component, Debug, Default)]
pub struct Lantern {
    pub lit: bool,
    pub flipped: bool,
    pub time: Stopwatch,
}

pub const LANTERN_RADIUS: f32 = 300.;

#[derive(Clone, Component)]
pub struct CanLightLanterns;

fn lantern_tile(
    mut commands: Commands,
    query: Query<(Entity, &Lantern), Changed<Lantern>>,
) {
    for (entity, lantern) in query.iter() {
        let uv = if lantern.lit {
            if lantern.time.elapsed_secs() > 0.1 {
                *crate::TILE_LANTERN3
            } else {
                *crate::TILE_LANTERN2
            }
        } else {
            *crate::TILE_LANTERN1
        };

        commands.entity(entity)
            .insert((
                AtlasTile {
                    quad: shape::Quad::new(Vec2::splat(16.)),
                    uv,
                    flip: BVec2::FALSE,
                },
                AtlasMaterialDefaultHandle,
                crate::renderer::AtlasHeightTile,
                YSorted(crate::LAYER_ABOVE_SPRITES),
                Collider::compound(vec![
                    (Vec2::new(0., -6.), 0., Collider::cuboid(2., 2.)),
                ]),
                RigidBody::Fixed,
            ));
    }
}

fn lantern_activate(
    mut commands: Commands,
    mut query_lanterns: Query<(Entity, &mut Lantern, &GlobalTransform), Without<Light>>,
    query_lighters: Query<&GlobalTransform, With<CanLightLanterns>>,
) {
    for (lantern_entity, mut lantern, lantern_transform) in query_lanterns.iter_mut() {
        for lighter_transform in query_lighters.iter() {
            if lighter_transform.translation().distance(lantern_transform.translation()) < 10. {
                lantern.lit = true;
                lantern.time.reset();

                commands.entity(lantern_entity)
                    .insert(
                        Light {
                            color: Color::rgb_u8(153, 250, 255),
                            radius: 0.,
                        }
                    );
            }
        }
    }
}

fn lantern_alpha_sine(
    mut query_lanterns: Query<(&mut Lantern, &mut Light)>,
    time: Res<Time>,
) {
    for (mut lantern, mut light) in query_lanterns.iter_mut() {
        lantern.time.tick(time.delta());
        light.color.set_a(lantern.time.elapsed_secs().sin() * 0.1 + 0.9);
        light.radius = (lantern.time.elapsed_secs() * LANTERN_RADIUS).clamp(0., LANTERN_RADIUS);
    }
}

pub struct LanternPlugin;

impl Plugin for LanternPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, (lantern_tile, lantern_activate, lantern_alpha_sine));
    }
}
