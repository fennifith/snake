use bevy::prelude::*;
use bevy::math::Vec3Swizzles;
use bevy_rapier2d::prelude::*;

use std::collections::{HashSet, VecDeque};
use crate::components::*;

#[derive(Clone, Component, Debug, Default)]
pub struct Candle {}
