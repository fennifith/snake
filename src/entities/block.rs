use bevy::ecs::schedule::SystemConfigs;
use bevy::prelude::*;
use bevy::math::Vec3Swizzles;
use bevy::sprite::{MaterialMesh2dBundle, Mesh2dHandle};
use bevy_rapier2d::prelude::*;

use std::collections::VecDeque;
use std::time::Duration;

use crate::components::InputMovement;
use crate::macros::console_log;

#[derive(Component, Default)]
pub struct Block;

fn block_spawn_from_ldtk(
	mut commands: Commands,
	query: Query<(Entity, &Transform), Added<Block>>,
) {
	for (entity, transform) in query.iter() {
		commands.entity(entity)
			.insert((
				SpriteBundle {
					sprite: Sprite {
						custom_size: Some(Vec2::splat(12.)),
						color: Color::rgba(0., 0., 1., 1.),
						..default()
					},
					transform: transform.clone(),
					..default()
				},
				Collider::cuboid(6., 6.),
				AdditionalMassProperties::Mass(250.0),
				RigidBody::Dynamic,
				Velocity::default(),
				Damping { linear_damping: 20.0, angular_damping: 40.0 },
			));
	}
}

pub fn block_systems() -> SystemConfigs {
	(block_spawn_from_ldtk)
		.chain()
}