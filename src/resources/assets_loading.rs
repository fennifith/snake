use bevy::prelude::*;
use bevy::asset::LoadState;

use crate::AppState;

#[derive(Resource, Default)]
pub struct AssetsLoading(pub Vec<HandleUntyped>);

pub fn check_assets_ready(
	mut commands: Commands,
	server: Res<AssetServer>,
	loading: Res<AssetsLoading>,
	mut next_state: ResMut<NextState<AppState>>,
) {
	match server.get_group_load_state(loading.0.iter().map(|h| h.id())) {
		LoadState::Failed => {
			// one of our assets had an error
		}
		LoadState::Loaded => {
			// all assets are now ready
			// transition to the in-game state
			next_state.set(AppState::Level);

			// remove the resource to drop the tracking handles
			commands.remove_resource::<AssetsLoading>();
			// (note: if you don't have any other handles to the assets
			// elsewhere, they will get unloaded after this)
		}
		_ => {
			// NotLoaded/Loading: not fully ready yet
		}
	}
}
