// https://github.com/bevyengine/bevy/blob/v0.11.0/crates/bevy_sprite/src/mesh2d/mesh2d.wgsl
#import bevy_sprite::mesh2d_vertex_output  MeshVertexOutput
#import bevy_sprite::mesh2d_functions as mesh_functions
#import bevy_sprite::mesh2d_view_bindings view, globals

@group(1) @binding(0)
var tex_color: texture_2d<f32>;
@group(1) @binding(1)
var tex_normal: texture_2d<f32>;
@group(1) @binding(2)
var tex_height: texture_2d<f32>;

@group(1) @binding(3)
var<uniform> light_colors: array<vec4f, 16>;
@group(1) @binding(4)
var<uniform> light_positions: array<vec4f, 16>;
@group(1) @binding(5)
var<uniform> light_count: i32;

@group(1) @binding(6)
var tex_view_height: texture_2d<f32>;
@group(1) @binding(7)
var tex_view_height_sampler: sampler;

fn position_world_to_texture(pos: vec4f) -> vec2f {
    return (view.view_proj * pos).xy * vec2f(.5, -.5) + .5;
}

fn get_shadow(
    trace_start: vec2f,
    trace_end: vec2f,
) -> f32 {
    var shadow: f32 = 1.;
    let diff = (trace_end - trace_start);

    let t_end = min(128., max(abs(diff.x), abs(diff.y)));
    let dt = diff / t_end;
    for (var t = 0.; t < t_end; t += 1.) {
        let sample_pos = trace_start + dt * t;
        let sample_uv = position_world_to_texture(vec4f(sample_pos, 0., 1.));
        let sample_color = textureSample(tex_view_height, tex_view_height_sampler, sample_uv);
        shadow *= 1. - sample_color.b;
    }

    return shadow;
}

fn get_light(
    world_position: vec4f,
    normal: vec4f,
    height: vec2f,
) -> vec4f {
    var ret = vec4<f32>(0.0);

    let norm = normalize(normal.xyz);

    let world_pos = world_position.xyz - vec3<f32>(height.xy, 0.);

    for (var i = 0; i < light_count; i++) {
        let light_color = light_colors[i];
        let light_position = floor(light_positions[i].xyz);
        let light_radius = light_positions[i].w;

        let trace_start = light_position.xy;
        let trace_end = world_position.xy;
        let trace_color = (
            get_shadow(trace_start, trace_end)
            + get_shadow(trace_start + vec2f(-1., -1.), trace_end)
            + get_shadow(trace_start + vec2f(1., -1.), trace_end)
            + get_shadow(trace_start + vec2f(-1., 1.), trace_end)
            + get_shadow(trace_start + vec2f(1., 1.), trace_end)
        ) / 5.;

        if (light_color.a > 0. && light_radius > 1.) {
            let distance = length((world_pos - light_position) * vec3<f32>(1., 2., 1.));
            let distance_linear = max((light_radius - distance) / light_radius, 0.);
            let light = light_color * pow(distance_linear, 3.) * light_color.a;

            let light_dir = normalize(world_pos - light_position);
            let norm_multiplier = mix(0., max(dot(norm, light_dir), 0.0), normal.w);

            ret += light * 0.02 + light * norm_multiplier * pow(trace_color, 2.);
        }
    }

    ret.a = 1.0;
    return ret;
}

// rgb(9, 20, 13)
const COLOR_GRASS = vec4f(0.035, 0.078, 0.051, 1.);
// rgb(5, 7, 22)
const COLOR_WATER = vec4f(0.020, 0.027, 0.086, 1.);

const CHUNK_SIZE = 128.;
const PI = 3.14159;

fn grass_wind(uv: vec2f) -> f32 {
    let sinY = sin((uv.y / CHUNK_SIZE) * PI * 2. + (globals.time * 0.5) % (PI * 2.));

    // let mod2 = (f32(uv.x) + 1. + sin((offsetX))) % 2.;
    return (sinY + 1.) / 2.;
}

fn grass_blade(uv: vec2f) -> f32 {
    let wind = grass_wind(uv) * (2. + sin(uv.x + uv.y / 4.));
    let wind_uv = vec2f(uv.x + wind, uv.y);

    return sin((wind_uv.x / 2.) * PI + (uv.y / 8.) * PI + (uv.y % 3.) * 16. + ((wind_uv.x + uv.y) % 5.) * 4.);
}

fn grass_color(uv: vec2f) -> vec4f {
    let blade = grass_blade(uv);
    let wind = grass_wind(uv);

    var color = COLOR_GRASS;
    color *= 0.95 + (blade * 0.1);
    color.b += wind * blade * 0.02;

    return color;
}

fn grass_normal(
    uv: vec2f,
    orig_normal: vec4f,
) -> vec4f {
    let blade = grass_blade(uv);
    let bladeX = grass_blade(uv + vec2f(1., 0.));
    let bladeY = grass_blade(uv + vec2f(0., 1.));

    let normal_xy = vec2f(bladeX - blade, 0.5 + bladeY - blade) * normalize(orig_normal.xyz).xy * 2.;
    return vec4f(normal_xy, -1., 1.);
}

fn is_border(uv: vec2u, color: vec4f) -> bool {
    let color1 = textureLoad(tex_color, uv + vec2u(1u, 0u), 0);
    let color2 = textureLoad(tex_color, uv + vec2u(0u, 1u), 0);
    var color3 = vec4f(0.);
    var color4 = vec4f(0.);
    if (uv.x > 0u) {
        color3 = textureLoad(tex_color, uv - vec2u(1u, 0u), 0);
    }
    if (uv.y > 0u) {
        color4 = textureLoad(tex_color, uv - vec2u(0u, 1u), 0);
    }

    return (color1.a > 0. && length(color - color1) > 0.01)
        || (color2.a > 0. && length(color - color2) > 0.01)
        || (color3.a > 0. && length(color - color3) > 0.01)
        || (color4.a > 0. && length(color - color4) > 0.01);
}

@fragment
fn fragment(
    in: MeshVertexOutput,
) -> @location(0) vec4<f32> {
    let uv = vec2<u32>(floor(in.uv) + 0.5);
    var color = textureLoad(tex_color, uv, 0);
    let normal_color = textureLoad(tex_normal, uv, 0);
    var normal = vec4<f32>(
        (1. - normal_color.rgb * 2.) * vec3<f32>(1., -1., 1.),
        normal_color.a,
    );

    // transform the normal value using tangent to account for rotation
    /*let N: vec3<f32> = in.world_normal;
    let T: vec3<f32> = in.world_tangent.xyz;
    let B: vec3<f32> = in.world_tangent.w * cross(N, T);
    normal = vec4<f32>(
        normalize(normal.x * T + normal.y * B + normal.z * N).xyz,
        normal.w,
    );*/

    /*let rotZ = mat3x3(
        cos(atlas_transform.x), -sin(atlas_transform.x), 0.,
        sin(atlas_transform.x), cos(atlas_transform.x), 0.,
        0., 0., 1.,
    );*/

    // return vec4<f32>(rotZ * normalize(normal.xyz), 1.);

    let height_color = textureLoad(tex_height, uv, 0);
    let height = vec2<f32>(
        (height_color.xy * 255. + 128.) % 255. - 128.
    );

    if (length(color - COLOR_GRASS) < 0.01) {
        if (is_border(uv, COLOR_GRASS)) {
            color *= 0.5;
        } else {
            color = grass_color(vec2f(uv));
            normal = grass_normal(vec2f(uv), normal);
        }
    } else if (length(color - COLOR_WATER) < 0.01) {
        if (is_border(uv, COLOR_WATER)) {
            color += vec4f(0.2, 0.2, 0.2, 0.);
        } else {
            // TODO: Water ripples/etc
        }
    }

    let light = get_light(in.world_position, normal, height);
    if (normal_color.x == 1. && normal_color.y == 1. && normal_color.z == 1. && normal_color.w == 1.) {
        return color;
    } else {
        return color * light;
    }
}
