#import bevy_sprite::mesh2d_view_bindings  view
#import bevy_sprite::mesh2d_vertex_output  MeshVertexOutput

@group(1) @binding(0)
var texture: texture_2d<f32>;

@group(1) @binding(1)
var texture_sampler: sampler;

@fragment
fn fragment(
    in: MeshVertexOutput,
) -> @location(0) vec4<f32> {
    let texture_size = vec2<f32>(textureDimensions(texture));
    let texture_pos = in.uv * texture_size;

    // prevent aliasing by sampling slightly between pixels
    let px_per_tx = view.viewport.zw / texture_size;
    let texture_offset = clamp(fract(texture_pos) * px_per_tx, vec2<f32>(0.), vec2<f32>(.5))
        - clamp((vec2<f32>(1.) - fract(texture_pos)) * px_per_tx, vec2<f32>(0.), vec2<f32>(.5));
    let texture_uv = floor(texture_pos) + 0.5 + texture_offset;

    var color = textureSample(texture, texture_sampler, texture_uv / texture_size);
    return color;
}
