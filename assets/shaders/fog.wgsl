#import bevy_render::globals Globals
#import bevy_core_pipeline::fullscreen_vertex_shader FullscreenVertexOutput
#import bevy_sprite::mesh2d_view_bindings view, globals
#import noisy_bevy simplex_noise_3d

// https://github.com/bevyengine/bevy/blob/v0.11.2/crates/bevy_core_pipeline/src/fullscreen_vertex_shader/fullscreen.wgsl
@vertex
fn vertex(@builtin(vertex_index) vertex_index: u32) -> FullscreenVertexOutput {
    let uv = vec2<f32>(f32(vertex_index >> 1u), f32(vertex_index & 1u)) * 2.0;
    let clip_position = vec4<f32>(uv * vec2<f32>(2.0, -2.0) + vec2<f32>(-1.0, 1.0), 0.0, 1.0);

    return FullscreenVertexOutput(clip_position, uv);
}

fn noise2Time(P: vec2f) -> f32 {
    return simplex_noise_3d(vec3<f32>(P.x, P.y, globals.time * 0.05));
}

@group(1) @binding(0)
var<uniform> light_colors: array<vec4f, 16>;
@group(1) @binding(1)
var<uniform> light_positions: array<vec4f, 16>;

fn get_light(
    world_pos: vec2f,
) -> vec4f {
    var ret = vec4<f32>(0.0);

    for (var i = 0; i < 16; i++) {
        let light_color = light_colors[i];
        let light_position = floor(light_positions[i].xy);
        let light_radius = light_positions[i].w * 0.5;

        if (light_color.a > 0. && light_radius > 1.) {
            let distance = length((world_pos - light_position) * vec2<f32>(1., 2.));
            let distance_linear = max((light_radius - distance) / light_radius, 0.);
            let light = light_color * pow(distance_linear, 3.) * light_color.a;
            ret += light;
        }
    }

    return ret;
}

@fragment
fn fragment(in: FullscreenVertexOutput) -> @location(0) vec4<f32> {
    let world_pos = in.uv * view.viewport.zw + view.world_position.xy * vec2<f32>(1., -1.) - view.viewport.zw / 2.;
    let noise_position = floor(in.uv * view.viewport.zw + view.world_position.xy * vec2<f32>(1., -1.)) + vec2<f32>(globals.time * 2., globals.time * 1.);

    let noise_value = noise2Time(noise_position * 0.02 * vec2<f32>(0.8, 4.));
    let noise_value2 = noise2Time(noise_position * 0.2 * vec2<f32>(0.12, 1.));

    let color = get_light(world_pos * vec2<f32>(1., -1.));

    // TODO: calculate color from lighting

    if (noise_value < 0.0) {
        let color_multiplier = abs(noise_value2);
        // let bloom_multiplier = 0.1 + max(0.0, noise_value2);
        let edge = max(noise_value + 0.5, 0.0);

        // TODO: set intensity uniform
        return vec4<f32>((color.rgb + 1.) / 2., (color_multiplier + edge) * color.a * 0.05);
    }

    return vec4<f32>(0., 0., 0., 0.);
}
