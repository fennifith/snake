// https://github.com/bevyengine/bevy/blob/v0.11.0/crates/bevy_sprite/src/mesh2d/mesh2d.wgsl
#import bevy_sprite::mesh2d_vertex_output  MeshVertexOutput

@group(1) @binding(0)
var tex_height: texture_2d<f32>;

@fragment
fn fragment(
    in: MeshVertexOutput,
) -> @location(0) vec4<f32> {
    let uv = vec2<u32>(floor(in.uv) + 0.5);
    let color = textureLoad(tex_height, uv, 0);
    return color;
}
