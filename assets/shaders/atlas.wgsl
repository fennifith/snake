// https://github.com/bevyengine/bevy/blob/v0.11.3/crates/bevy_sprite/src/mesh2d/mesh2d.wgsl
#import bevy_sprite::mesh2d_vertex_output  MeshVertexOutput
#import bevy_sprite::mesh2d_functions as mesh_functions
#import bevy_sprite::mesh2d_bindings       mesh
#import bevy_sprite::mesh2d_view_bindings  view

struct Vertex {
    @location(0) position: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) uv: vec2<f32>,
};

@vertex
fn vertex(vertex: Vertex) -> MeshVertexOutput {
    var out: MeshVertexOutput;
    out.uv = vertex.uv;

    out.world_position = mesh_functions::mesh2d_position_local_to_world(
        mesh.model,
        vec4<f32>(vertex.position, 1.0)
    );
    out.position = mesh_functions::mesh2d_position_world_to_clip(out.world_position);

    out.world_normal = mesh_functions::mesh2d_normal_local_to_world(vertex.normal);

    return out;
}

@group(1) @binding(0)
var tex_color: texture_2d<f32>;
@group(1) @binding(1)
var tex_normal: texture_2d<f32>;
@group(1) @binding(2)
var tex_height: texture_2d<f32>;

@group(1) @binding(3)
var<uniform> light_colors: array<vec4f, 16>;
@group(1) @binding(4)
var<uniform> light_positions: array<vec4f, 16>;

@group(1) @binding(5)
var tex_view_height: texture_2d<f32>;

fn position_world_to_texture(pos: vec4f) -> vec2f {
    let clip_pos = (mesh_functions::mesh2d_position_world_to_clip(pos).xy * vec2f(1., -1.) + 1.) / 2.;
    return clip_pos * view.viewport.zw;
}

fn get_shadow(
    trace_start: vec2f,
    trace_end: vec2f,
) -> f32 {
    var shadow: f32 = 1.;
    let diff = (trace_end - trace_start);

    let t_end = min(128., max(abs(diff.x), abs(diff.y)));
    let dt = diff / t_end;
    for (var t = 0.; t < t_end; t += 1.) {
        let sample_pos = trace_start + dt * t;
        let sample_pos_i = vec2u(floor(sample_pos.xy));
        let sample_color = textureLoad(tex_view_height, sample_pos_i, 0);
        shadow *= 1. - sample_color.b;
    }

    return shadow;
}

fn get_light(
    world_position: vec4f,
    normal: vec4f,
    height: vec2f,
) -> vec4f {
    var ret = vec4<f32>(0.0);

    let norm = normalize(normal.xyz);

    let world_pos = world_position.xyz - vec3<f32>(height.xy, 0.);

    let trace_end = position_world_to_texture(world_position);

    for (var i = 0; i < 16; i++) {
        let light_color = light_colors[i];
        let light_position = floor(light_positions[i].xyz);
        let light_radius = light_positions[i].w;

        let trace_start = position_world_to_texture(vec4f(light_position, 1.));
        // let trace_color = get_shadow(trace_start, trace_end);

        if (light_color.a > 0. && light_radius > 1.) {
            let distance = length((world_pos - light_position) * vec3<f32>(1., 2., 1.));
            let distance_linear = max((light_radius - distance) / light_radius, 0.);
            let light = light_color * pow(distance_linear, 3.) * light_color.a;

            let light_dir = normalize(world_pos - light_position);
            let norm_multiplier = mix(0., max(dot(norm, light_dir), 0.0), normal.w);

            ret += light * 0.02 + light * norm_multiplier; // * trace_color;
        }

        // if (length(trace_color.rgb) > 3.) {
        //     ret.r = 1.;
        // }
    }

    ret.a = 1.0;
    return ret;
}

@fragment
fn fragment(
    in: MeshVertexOutput,
) -> @location(0) vec4<f32> {
    let uv = vec2<u32>(floor(in.uv) + 0.5);
    let color = textureLoad(tex_color, uv, 0);
    let normal_color = textureLoad(tex_normal, uv, 0);
    var normal = vec4<f32>(
        (1. - normal_color.rgb * 2.) * vec3<f32>(1., -1., 1.),
        normal_color.a,
    );

    let height_color = textureLoad(tex_height, uv, 0);
    let height = vec2<f32>(
        (height_color.xy * 255. + 128.) % 255. - 128.
    );
    let light = get_light(in.world_position, normal, height);

    // let pos = mesh_functions::mesh2d_position_world_to_clip(in.world_position);

    if (normal_color.x == 1. && normal_color.y == 1. && normal_color.z == 1. && normal_color.w == 1.) {
        return color;
    } else {
        return color * light;
    }
}
