web:
	cargo build --target wasm32-unknown-unknown
	wasm-bindgen --out-dir ./public/ --target web ./target/wasm32-unknown-unknown/debug/*.wasm
	mkdir -p ./public/assets && cp -rf ./assets/* ./public/assets/
	cd public && npx http-server
